//
//  InterviewCell.h
//  NotesiOS
//
//  Created by vivek athilkar on 03/01/17.
//  Copyright © 2017 vivek athilkar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InterviewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *interviewQuestion;

@end
