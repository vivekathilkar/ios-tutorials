//
//  InterviewVC.h
//  NotesiOS
//
//  Created by vivek athilkar on 03/01/17.
//  Copyright © 2017 vivek athilkar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InterviewVC : UIViewController <UITableViewDelegate,UITableViewDataSource>


@property (weak, nonatomic) IBOutlet UITableView *tableView;



@end
