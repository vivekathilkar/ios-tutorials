//
//  NotesCell.h
//  NotesiOS
//
//  Created by vivek athilkar on 27/12/16.
//  Copyright © 2016 vivek athilkar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotesCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *cellLabel;


@end
