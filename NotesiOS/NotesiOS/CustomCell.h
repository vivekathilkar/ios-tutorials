//
//  CustomCell.h
//  NotesiOS
//
//  Created by vivek athilkar on 18/12/16.
//  Copyright © 2016 vivek athilkar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@property (weak, nonatomic) IBOutlet UIButton *like;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *likeTopconstraint;
-(void)addConstrain;

-(void)addConstrainaa;


@property (weak, nonatomic) IBOutlet UIButton *moreButton;

@property (weak, nonatomic) IBOutlet UIImageView *arrowImage;



@end
