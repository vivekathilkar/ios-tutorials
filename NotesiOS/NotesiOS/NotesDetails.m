//
//  NotesDetails.m
//  NotesiOS
//
//  Created by vivek athilkar on 09/12/16.
//  Copyright © 2016 vivek athilkar. All rights reserved.
//
//pragma foreign_keys = on;

#import "NotesDetails.h"

@implementation NotesDetails
@synthesize webView;

-(void)viewDidLoad
{
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    NSLog(@"%@",_rowName);
    [self setTitle:_rowName];
    NSString *urlAddress = [[NSBundle mainBundle]pathForResource:_rowName ofType:@"html"];
    NSURL *url = [NSURL fileURLWithPath:urlAddress];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [webView loadRequest:requestObj];
    webView.scrollView.showsHorizontalScrollIndicator = false;
    self.webView.scrollView.decelerationRate = UIScrollViewDecelerationRateNormal;
    webView.scrollView.showsVerticalScrollIndicator = NO;
    webView.scrollView.showsHorizontalScrollIndicator = NO;
    _activityIndicator.hidesWhenStopped = YES;
    
}

-(void)webViewDidFinishLoad:(UIWebView *)webViewview{
    [webViewview scalesPageToFit];
    float scale = 90000/webViewview.scrollView.frame.size.width;
    NSString *jsString = [[NSString alloc] initWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '%f%%'", scale];
    [webViewview stringByEvaluatingJavaScriptFromString:jsString];
    [self stopAnimating];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    
    self.activityIndicator.startAnimating;
}

-(void)stopAnimating
{
    self.activityIndicator.stopAnimating;
}

-(void)viewWillDisappear:(BOOL)animated
{
    if([webView isLoading])
    {
        [webView stopLoading];
    }
}


@end
