//
//  LevelSelection.m
//  NotesiOS
//
//  Created by vivek athilkar on 16/12/16.
//  Copyright © 2016 vivek athilkar. All rights reserved.
//

#import "LevelSelection.h"
#import "Question.h"

@interface LevelSelection ()

@property (weak, nonatomic) IBOutlet UIButton *level1;
@property (weak, nonatomic) IBOutlet UIButton *level2;
@property (weak, nonatomic) IBOutlet UIButton *level3;
@property (weak, nonatomic) IBOutlet UIButton *level4;
@property (weak, nonatomic) IBOutlet UIButton *level5;
@property (weak, nonatomic) IBOutlet UIView *borderView;


@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic,strong) Question *queObj;
@end

@implementation LevelSelection

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.scrollView.layer.cornerRadius = 7;
    self.scrollView.layer.masksToBounds = true;
    _queObj = [[Question alloc]init];
    
    _level1.layer.cornerRadius = 13; // this value vary as per your desire
    _level1.clipsToBounds = YES;
    
    _level2.layer.cornerRadius = 13; // this value vary as per your desire
    _level2.clipsToBounds = YES;
    
    _level3.layer.cornerRadius = 13; // this value vary as per your desire
    _level3.clipsToBounds = YES;
    
    _level4.layer.cornerRadius = 13; // this value vary as per your desire
    _level4.clipsToBounds = YES;
    
    _level5.layer.cornerRadius = 13; // this value vary as per your desire
    _level5.clipsToBounds = YES;
    [_scrollView setShowsVerticalScrollIndicator:NO];
    _borderView.layer.cornerRadius = 7;
    _borderView.layer.masksToBounds = TRUE;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:TRUE];

    [_level2 setEnabled:NO];
    [_level3 setEnabled:NO];
    [_level4 setEnabled:NO];
    [_level5 setEnabled:NO];

    //NSLog(@"%ld",(long)[[Question thePlayer]score]);
    
    NSInteger *firstlevelScore = (NSInteger*)[[NSUserDefaults standardUserDefaults]integerForKey:@"firstlevelScore"];
    NSInteger *secondlevelScore = (NSInteger*)[[NSUserDefaults standardUserDefaults]integerForKey:@"secondlevelScore"];
    NSInteger *thirdlevelScore = (NSInteger*)[[NSUserDefaults standardUserDefaults]integerForKey:@"thirdlevelScore"];
    NSInteger *fourthlevelScore = (NSInteger*)[[NSUserDefaults standardUserDefaults]integerForKey:@"fourthlevelScore"];
    
    if(firstlevelScore == (NSInteger*)3)
    {
        _level1.highlighted = NO;
        _level2.highlighted = NO;
        _level3.highlighted = YES;
        _level4.highlighted = YES;
        _level5.highlighted = YES;
        
        [_level1 setEnabled:YES];
        [_level2 setEnabled:YES];
        [_level3 setEnabled:NO];
        [_level4 setEnabled:NO];
        [_level5 setEnabled:NO];
        
        if(secondlevelScore == (NSInteger*)3)
        {
            _level2.highlighted = NO;
            _level3.highlighted = NO;
            _level4.highlighted = YES;
            _level5.highlighted = YES;
            
            [_level2 setEnabled:YES];
            [_level3 setEnabled:YES];
            [_level4 setEnabled:NO];
            [_level5 setEnabled:NO];
            
            if(thirdlevelScore == (NSInteger*)3)
            {
                _level3.highlighted = NO;
                _level4.highlighted = NO;
                _level5.highlighted = YES;
                
                [_level3 setEnabled:YES];
                [_level4 setEnabled:YES];
                [_level5 setEnabled:NO];
                
                if(fourthlevelScore == (NSInteger*)3)
                {
                    _level4.highlighted = NO;
                    _level5.highlighted = NO;
                    
                    [_level4 setEnabled:YES];
                    [_level5 setEnabled:YES];
                    
                }

                
            }
            
        }
    }
    
    
   
    [Question resetSharedInstance];

}

-(IBAction)buttonPressed:(id)sender
{
    [self performSegueWithIdentifier:@"levelButtonSegue" sender:sender];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(UIButton*)sender
{
    if([[segue identifier]isEqualToString:@"levelButtonSegue"])
    {
        Question *queVC = [segue destinationViewController];
        NSLog(@"%ld",sender.tag);
        queVC.levelButtonTag = sender.tag;
        [queVC buttonTag:sender];
        
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/





@end
