//
//  Notes.m
//  NotesiOS
//
//  Created by vivek athilkar on 19/12/16.
//  Copyright © 2016 vivek athilkar. All rights reserved.
//

#import "Notes.h"
#import "CustomCell.h"
#import <FMDatabase.h>
#import "NotesDetails.h"
#import "NotesCell.h"

static NSString *const kTableViewCellReuseIdentifier = @"TableViewCellReuseIdentifier";

@interface Notes ()
{
    BOOL openclose;

}

@property (strong, nonatomic) IBOutlet FZAccordionTableView *tableView;
@property (strong, nonatomic)  UIImageView *img;
@property (strong, nonatomic)  UILabel *label;

@end

@implementation Notes
@synthesize introduction,xcode,objectiveC,appPattern,viewWindows,storyBoardss,tableViewss,navigationBased,Uipicker,directoriesFile,workingWithdata,multiTouchTap,drawing,animation,multasking,notifications,corelocations,consurrency,networking,targetingDevices,liclization,PerfPowerOpt;

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.tableView.sectionHeaderHeight = UITableViewAutomaticDimension;
//    self.tableView.estimatedSectionHeaderHeight = 25;
    openclose = FALSE;
    // Do any additional setup after loading the view.
    _mutableArray = [[NSMutableArray alloc]init];
    _tableDataArray = [[NSMutableArray alloc]init];
    
    introduction = [[NSMutableArray alloc]init];
    xcode = [[NSMutableArray alloc]initWithObjects:@"Compiling Objective-C from the Command Line",@"Installing Xcode on Mac OS X",@"Starting Xcode",@"Writing an Objective-C Application with Xcode",nil];
    objectiveC = [[NSMutableArray alloc]initWithObjects:@"Automatic Reference Counting",@"Blocks",@"Categories and Extensions",@"Classes, Objects, and Methods",@"Formal and Informal Protocols",@"Memory Management",nil];
    appPattern = [[NSMutableArray alloc]initWithObjects:@"IBOutlets, IBActions and IBOutletCollection",@"Model View Controller",@"Subclassing and Delegation",nil];
    viewWindows = [[NSMutableArray alloc]initWithObjects:@"Alert Views and Action Sheets",@"Autolayout",@"Containers",@"Controlling Rotation Behavior",@"Controls",@"Text and Web Views",@"The View Hierarchy",@"View Autosizing", nil];
    storyBoardss = [[NSMutableArray alloc]initWithObjects:@"Adding Scenes",@"Associating a View Controller with a Scene",@"Configuring Storyboard Segues",@"Configuring Storyboard Transitions",@"Introduction",@"Passing Data Between Scenes",nil];
    tableViewss = [[NSMutableArray alloc]initWithObjects:@"Custom Cells",@"Delegates and DataSources",@"Introduction",@"Static and Dynamic Table Views",@"Table View Styles",nil];
    navigationBased = [[NSMutableArray alloc]initWithObjects:@"Adding the Root View Controller",@"Controlling the Stack Navigation",@"Creating the Navigation Controller",nil];
    Uipicker = [[NSMutableArray alloc]initWithObjects:@"Coding for the Date Picker",@"Designing the UI",@"Hiding the Keyboard",nil];
    directoriesFile = [[NSMutableArray alloc]initWithObjects:@"Archiving",@"iCloud",@"Key-V alue Data",@"NSFileManager, NSFileHandle, and NSData",@"Pathnames in Objective-C",@"Working with Directories",@"Working with Files",nil];
    workingWithdata = [[NSMutableArray alloc]initWithObjects:@"Entity Descriptions",@"Manage Object",@"Managed Object Model",@"Overview of Core Data",@"Persistent Store Coordinator",@"Retrieving and Modifying Data ",@"SQLite Integration",nil];
    multiTouchTap = [[NSMutableArray alloc]initWithObjects:@"",@"",@"",@"",@"",@"",@"",@"", nil];
    drawing = [[NSMutableArray alloc]initWithObjects:@"",@"",@"",@"",@"",@"",@"",@"", nil];
    animation = [[NSMutableArray alloc]initWithObjects:@"Animation Curves",@"Core Animation Blocks",@"Transformations",nil];
    multasking = [[NSMutableArray alloc]initWithObjects:@"",@"",@"",@"",@"",@"",@"",@"", nil];
    notifications = [[NSMutableArray alloc]initWithObjects:@"",@"",@"",@"",@"",@"",@"",@"", nil];
    corelocations = [[NSMutableArray alloc]initWithObjects:@"Calculating Distances",@"Location Accuracy",@"MapKit Framework and MKMapView",@"Obtaining Location Information",nil];
    consurrency = [[NSMutableArray alloc]initWithObjects:@"Completion Blocks",@"Grand Central Dispatch (GCD)",@"Operation Queues",nil];
    networking = [[NSMutableArray alloc]initWithObjects:@"AirDrop",@"Asynchronous Downloads",@"Handling Timeouts",@"Parsing JSON",@"Parsing XML",@"Reachability",@"Sending HTTP GET and POST Requests",@"Synchronous Downloads", nil];
    targetingDevices = [[NSMutableArray alloc]initWithObjects:@"iPhone vs. iPad",@"Multiple SDK Support",@"Supporting iOS 6 and iOS 7",@"Detecting Device Capabilities",@"Universal Apps",nil];
    liclization = [[NSMutableArray alloc]init];
    PerfPowerOpt = [[NSMutableArray alloc]init];
    
    _stpkfintroduction = [[NSMutableArray alloc]init];
    _stpkxcode = [[NSMutableArray alloc]init];
     selectedIndex = -1;
//    [self createTable];
    //    [self insertDataInDB];
    [self getAllDataFromDB];
    [self getSubtitleDB];
    //    [self updateDataInDB];
    self.tableView.layer.cornerRadius = 7;
    self.tableView.layer.masksToBounds = true;
    self.tableView.allowMultipleSectionsOpen = NO;
//    self.tableView.keepOneSectionOpen = YES;
//    self.tableView.initialOpenSections = [NSSet setWithObjects:@(0), nil];
    self.tableView.scrollEnabled = YES;
    self.tableView.showsVerticalScrollIndicator = NO;
    
//    self.tableView.separatorStyle = UITableViewStylePlain;
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:kTableViewCellReuseIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:@"AccordionHeaderView" bundle:nil] forHeaderFooterViewReuseIdentifier:kAccordionHeaderViewReuseIdentifier];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"NotesCell" bundle:nil] forCellReuseIdentifier:@"NotesCell"];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//NEW CODE

#pragma mark - <UITableViewDataSource> / <UITableViewDelegate> -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  
    switch(section)
    {
        case 0:
            return introduction.count;
            break;
        case 1:
            return xcode.count;
            break;
        case 2:
            return objectiveC.count;
            break;
        case 3:
            return appPattern.count;
            break;
        case 4:
            return viewWindows.count;
            break;
        case 5:
            return storyBoardss.count;
            break;
        case 6:
            return tableViewss.count;
            break;
        case 7:
            return navigationBased.count;
            break;
        case 8:
            return Uipicker.count;
            break;
        case 9:
            return directoriesFile.count;
            break;
        case 10:
            return workingWithdata.count;
            break;
        case 11:
            return multiTouchTap.count;
            break;
        case 12:
            return drawing.count;
            break;
        case 13:
            return animation.count;
            break;
        case 14:
            return multasking.count;
            break;
        case 15:
            return notifications.count;
            break;
        case 16:
            return corelocations.count;
            break;
        case 17:
            return consurrency.count;
            break;
        case 18:
            return networking.count;
            break;
        case 19:
            return targetingDevices.count;
            break;
        case 20:
            return liclization.count;
            break;
        case 21:
            return PerfPowerOpt.count;
            break;

        default:
            return 2;
            break;
    }}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _mutableArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return kDefaultAccordionHeaderViewHeight;
}

//- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    return [self tableView:tableView heightForRowAtIndexPath:indexPath];
//}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section {
    return [self tableView:tableView heightForHeaderInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.section >= 0 && indexPath.section <= 20)
    {
        //CAN USE A CUSTOM CELL HERE CAN CHANGE CELL HEIGHT ALSO
        NotesCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NotesCell" forIndexPath:indexPath];
        [self setUpCell:cell atIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
        
    }
       else
    {
        NotesCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NotesCell" forIndexPath:indexPath];
        cell.cellLabel.text = @"hello";
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }

}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
//    return [tableView dequeueReusableHeaderFooterViewWithIdentifier:kAccordionHeaderViewReuseIdentifier];
    
//    static NSString *HeaderIdentifier = @"header";
//    
//    UITableViewHeaderFooterView *myHeader = [tableView dequeueReusableHeaderFooterViewWithIdentifier:HeaderIdentifier];
//    if(!myHeader) {
//        myHeader = [[UITableViewHeaderFooterView alloc] initWithReuseIdentifier:HeaderIdentifier];
//    }
//    
//    UIButton *btnUp = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//    [btnUp setTag:101];
//    [btnUp setTitle:@"-" forState:UIControlStateNormal];
//    [btnUp setFrame:CGRectMake(tableView.frame.size.width - 35, 5, 30, 30)];
//    [myHeader addSubview:btnUp];
//    
//    [myHeader.textLabel setText:[NSString stringWithFormat:@"Section: %ld",(long)section]];
//    
//    [myHeader setFrame:CGRectMake(0, 0, tableView.frame.size.width, 50)];
//    return myHeader;
    
    static NSString *HeaderIdentifier = @"header";
    AccordionHeaderView *myHeader = [tableView dequeueReusableHeaderFooterViewWithIdentifier:HeaderIdentifier];
    if(!myHeader) {
        //    [tableView registerClass:[CustomHeaderView class] forHeaderFooterViewReuseIdentifier:HeaderIdentifier];
        myHeader = [[[NSBundle mainBundle] loadNibNamed:@"AccordionHeaderView"
                                                  owner:self
                                                options:nil] objectAtIndex:0];
    }
    
//    [myHeader.label setTitle:@"-" forState:UIControlStateNormal];
    [myHeader.label setText:[_mutableArray objectAtIndex:section]];
    return myHeader;
}


#pragma mark - <FZAccordionTableViewDelegate> -

- (void)tableView:(FZAccordionTableView *)tableView willOpenSection:(NSInteger)section withHeader:(UITableViewHeaderFooterView *)header {
    

}

- (void)tableView:(FZAccordionTableView *)tableView didOpenSection:(NSInteger)section withHeader:(UITableViewHeaderFooterView *)header {
    
}

- (void)tableView:(FZAccordionTableView *)tableView willCloseSection:(NSInteger)section withHeader:(UITableViewHeaderFooterView *)header
{
    

}

- (void)tableView:(FZAccordionTableView *)tableView didCloseSection:(NSInteger)section withHeader:(UITableViewHeaderFooterView *)header {
    

}

- (void)setUpCell:(NotesCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.section ==0)
    {
       
    cell.cellLabel.text = [self.introduction objectAtIndex:indexPath.row];
    }
    if(indexPath.section ==1)
    {
        
        cell.cellLabel.text = [self.xcode objectAtIndex:indexPath.row];
    }
    if(indexPath.section ==2)
    {
        
        cell.cellLabel.text = [self.objectiveC objectAtIndex:indexPath.row];
    }
    if(indexPath.section ==3)
    {
        
        cell.cellLabel.text = [self.appPattern objectAtIndex:indexPath.row];
    }
    if(indexPath.section ==4)
    {
        
        cell.cellLabel.text = [self.viewWindows objectAtIndex:indexPath.row];
    }
    if(indexPath.section ==5)
    {
        
        cell.cellLabel.text = [self.storyBoardss objectAtIndex:indexPath.row];
    }
    if(indexPath.section ==6)
    {
        
        cell.cellLabel.text = [self.tableViewss objectAtIndex:indexPath.row];
    }
    if(indexPath.section ==7)
    {
        
        cell.cellLabel.text = [self.navigationBased objectAtIndex:indexPath.row];
    }
    if(indexPath.section ==8)
    {
        
        cell.cellLabel.text = [self.Uipicker objectAtIndex:indexPath.row];
    }
    if(indexPath.section ==9)
    {
        
        cell.cellLabel.text = [self.directoriesFile objectAtIndex:indexPath.row];
    }
    if(indexPath.section ==10)
    {
        
        cell.cellLabel.text = [self.workingWithdata objectAtIndex:indexPath.row];
    }
    if(indexPath.section ==11)
    {
        
        cell.cellLabel.text = [self.multiTouchTap objectAtIndex:indexPath.row];
    }
    if(indexPath.section ==12)
    {
        
        cell.cellLabel.text = [self.drawing objectAtIndex:indexPath.row];
    }
    if(indexPath.section ==13)
    {
        
        cell.cellLabel.text = [self.animation objectAtIndex:indexPath.row];
    }
    if(indexPath.section ==14)
    {
        
        cell.cellLabel.text = [self.multasking objectAtIndex:indexPath.row];
    }
    if(indexPath.section ==15)
    {
        
        cell.cellLabel.text = [self.notifications objectAtIndex:indexPath.row];
    }
     if(indexPath.section ==16)
    {
        
        cell.cellLabel.text = [self.corelocations objectAtIndex:indexPath.row];
    }
    if(indexPath.section ==17)
    {
        
        cell.cellLabel.text = [self.consurrency objectAtIndex:indexPath.row];
    }
    if(indexPath.section ==18)
    {
        
        cell.cellLabel.text = [self.networking objectAtIndex:indexPath.row];
    }
    if(indexPath.section ==19)
    {
        
        cell.cellLabel.text = [self.targetingDevices objectAtIndex:indexPath.row];
    }
    if(indexPath.section ==20)
    {
        
        cell.cellLabel.text = [self.liclization objectAtIndex:indexPath.row];
    }
    if(indexPath.section ==21)
    {
        
        cell.cellLabel.text = [self.PerfPowerOpt objectAtIndex:indexPath.row];
    }
    
}


//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    static NotesCell *cell = nil;
//    static dispatch_once_t onceToken;
//    
//    dispatch_once(&onceToken, ^{
//        cell = [self.tableView dequeueReusableCellWithIdentifier:@"NotesCell"];
//    });
//    
//    [self setUpCell:cell atIndexPath:indexPath];
//    
//    return [self calculateHeightForConfiguredSizingCell:cell];
//}
//
//- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell {
//    [sizingCell layoutIfNeeded];
//    
//    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
//    return size.height;
//}


//NEW CODE IS UPTO HERE
//OLD CODE
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    return 1;
//}

//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    return _mutableArray.count;
//}
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    static NSString *cellIdentifier1 = @"CustomCell";
//    CustomCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier1];
//    if(cell == nil)
//    {
//        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"CustomCell" owner:self options:nil];
//        cell = [nib objectAtIndex:0];
//    }
//    NSLog(@"%lu%lu",_mutableArray.count,_tableDataArray.count);
//    cell.titleLabel.text = [_mutableArray objectAtIndex:indexPath.row];
//    cell.descriptionLabel.text = [_tableDataArray objectAtIndex:indexPath.row];
//    if(selectedIndex == indexPath.row)
//    {
//    cell.descriptionLabel.hidden = FALSE;
//        cell.moreButton.hidden = FALSE;
//        cell.arrowImage.image = [UIImage imageNamed:@"ic_down_arrow_36pt_1x.png"];
//    }
//    else{
//        cell.descriptionLabel.hidden = TRUE;
//        cell.moreButton.hidden = TRUE;
//        cell.arrowImage.image = [UIImage imageNamed:@"ic_right_arrow_36pt_1x.png"];
//    }
//    
//    
//    buttoncell = cell.moreButton;
//    cell.moreButton.tag = indexPath.row;
//    buttoncell.tag = indexPath.row;
//        cell.moreButton.layer.cornerRadius = 13;
//    cell.moreButton.layer.masksToBounds = true;
//    //    [cell.layer setCornerRadius:10.0f];
////        [cell.layer setMasksToBounds:YES];
////        [cell.layer setBorderWidth:1.0f];
////        cell.layer.borderColor = [[UIColor lightGrayColor] CGColor];
//    cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//    [tableView setShowsVerticalScrollIndicator:NO];
//
//    
//    cell.clipsToBounds = YES;
//    
//    return cell;
//}

//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//    if(selectedIndex == indexPath.row)
//    {
//        return 240;
//        
//    }
//    else
//    {
//        return 45;
//    }
//}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
//    NSIndexPath *selected = [tableView indexPathForSelectedRow];
 
    if(indexPath.section ==0)
    {
        
        selectedRow = [introduction objectAtIndex:(long)indexPath.row];
    }
    if(indexPath.section ==1)
    {
        
        selectedRow = [xcode objectAtIndex:(long)indexPath.row];
    }
    if(indexPath.section ==2)
    {
        
        selectedRow = [objectiveC objectAtIndex:(long)indexPath.row];
    }
    if(indexPath.section ==3)
    {
        
        selectedRow = [appPattern objectAtIndex:(long)indexPath.row];
    }
    if(indexPath.section ==4)
    {
        
        selectedRow = [viewWindows objectAtIndex:(long)indexPath.row];
    }
    if(indexPath.section ==5)
    {
        
        selectedRow = [storyBoardss objectAtIndex:(long)indexPath.row];
    }
    if(indexPath.section ==6)
    {
        
        selectedRow = [tableViewss objectAtIndex:(long)indexPath.row];
    }
    if(indexPath.section ==7)
    {
        
        selectedRow = [navigationBased objectAtIndex:(long)indexPath.row];
    }
    if(indexPath.section ==8)
    {
        
        selectedRow = [Uipicker objectAtIndex:(long)indexPath.row];
    }
    if(indexPath.section ==9)
    {
        
        selectedRow = [directoriesFile objectAtIndex:(long)indexPath.row];
    }
    if(indexPath.section ==10)
    {
        
        selectedRow = [workingWithdata objectAtIndex:(long)indexPath.row];
    }
    if(indexPath.section ==11)
    {
        
        selectedRow = [multiTouchTap objectAtIndex:(long)indexPath.row];
    }
    if(indexPath.section ==12)
    {
        
        selectedRow = [drawing objectAtIndex:(long)indexPath.row];
    }
    if(indexPath.section ==13)
    {
        
        selectedRow = [animation objectAtIndex:(long)indexPath.row];
    }
    if(indexPath.section ==14)
    {
        
        selectedRow = [multasking objectAtIndex:(long)indexPath.row];
    }
    if(indexPath.section ==15)
    {
        
        selectedRow = [notifications objectAtIndex:(long)indexPath.row];
    }
    if(indexPath.section ==16)
    {
        
        selectedRow = [corelocations objectAtIndex:(long)indexPath.row];
    }
    if(indexPath.section ==17)
    {
        
        selectedRow = [consurrency objectAtIndex:(long)indexPath.row];
    }
    if(indexPath.section ==18)
    {
        
        selectedRow = [networking objectAtIndex:(long)indexPath.row];
    }
    if(indexPath.section ==19)
    {
        
        selectedRow = [targetingDevices objectAtIndex:(long)indexPath.row];
    }
    if(indexPath.section ==20)
    {
        
        selectedRow = [liclization objectAtIndex:(long)indexPath.row];
    }
    if(indexPath.section ==21)
    {
        
        selectedRow = [PerfPowerOpt objectAtIndex:(long)indexPath.row];
    }

    
    [self performSegueWithIdentifier:@"noteDetail" sender:self];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([[segue identifier]isEqualToString:@"noteDetail"])
    {
        NotesDetails *notesDetail = [segue destinationViewController];
        notesDetail.rowName = selectedRow;
    }
}

//OLD CODE IS UPTO TO HERE
//- (void)createTable {
//    // Getting the database path.
//    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *docsPath = [paths objectAtIndex:0];
//    NSString *dbPath = [docsPath stringByAppendingPathComponent:@"sqldb.sqlite"];
//    
//    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
//    [database open];
//    [database executeUpdate:@"create table if not exists college(name varchar primary key, ID int)"];
//    [database close];
//}
//
//
//- (void)insertDataInDB {
//    
//    // Getting the database path.
//    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *docsPath = [paths objectAtIndex:0];
//    NSString *dbPath = [docsPath stringByAppendingPathComponent:@"sqldb.sqlite"];
//    
//    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
//    [database open];
//    NSString *insertQuery = [NSString stringWithFormat:@"INSERT INTO college VALUES ('%@', %d)", @"PCE College", 1];
//    [database executeUpdate:insertQuery];
//    [database close];
//}
//

- (void)getAllDataFromDB {
    // Getting the database path.
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *dbPath = [docsPath stringByAppendingPathComponent:@"db.sqlite"];
    NSLog(@"%@",dbPath);
    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    NSString *sqlSelectQuery = @"SELECT * FROM topic";
    
    // Query result
    FMResultSet *resultsWithCollegeName = [database executeQuery:sqlSelectQuery];
    while([resultsWithCollegeName next]) {
        NSString *id = [NSString stringWithFormat:@"%d",[resultsWithCollegeName intForColumn:@"id"]];
        NSString *title = [NSString stringWithFormat:@"%@",[resultsWithCollegeName stringForColumn:@"title"]];
        
        [_mutableArray addObject:title];
//        NSLog(@"%lu%lu%@%@",_mutableArray.count,_tableDataArray.count,title,description);
                [_tableView reloadData];

        // save your fetch data into the array, dictionaries.
        //        NSLog(@"ID = %@, note = %@ description = %@ ",id, note,description);
    }
    [database close];
    NSLog(@"%lu",(unsigned long)_mutableArray.count);
}

- (void)getSubtitleDB {
    // Getting the database path.
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *dbPath = [docsPath stringByAppendingPathComponent:@"db.sqlite"];
    NSLog(@"%@",dbPath);
    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    NSString *sqlSelectQuery = @"SELECT * FROM subtopic";
    
    // Query result
    FMResultSet *resultsWithCollegeName = [database executeQuery:sqlSelectQuery];
    while([resultsWithCollegeName next]) {
        NSString *id = [NSString stringWithFormat:@"%d",[resultsWithCollegeName intForColumn:@"idd"]];
        NSString *subtitle = [NSString stringWithFormat:@"%@",[resultsWithCollegeName stringForColumn:@"subtitle"]];
//        NSString *fkey = [NSString stringWithFormat:@"%d",[resultsWithCollegeName intForColumn:@"fkey"]];
        [_tableView reloadData];
        if([resultsWithCollegeName intForColumn:@"fkey"] == 1001)
        {
//            [introduction addObject:subtitle];
            [_stpkfintroduction addObject:id];
        }
        if([resultsWithCollegeName intForColumn:@"fkey"] == 1002)
        {
//            [xcode addObject:subtitle];
            [_stpkxcode addObject:id];
        }
        
    }
    [database close];
    NSLog(@"%lu",(unsigned long)_mutableArray.count);
}


//- (void)updateDataInDB
//{
//    
//    // Getting the database path.
//    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *docsPath = [paths objectAtIndex:0];
//    NSString *dbPath = [docsPath stringByAppendingPathComponent:@"sqldb.sqlite"];
//    
//    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
//    [database open];
//    NSString *updateQuery = [NSString stringWithFormat:@"UPDATE college SET ID = '%@' WHERE name = '%@'", @"2", @"PCE College" ];
//    [database executeUpdate:updateQuery];
//    [self getAllDataFromDB];
//    [database close];
//    
//}
//
//#import "FMDatabase.h"
//
//- (void)deleteDataInDB {
//    
//    // Getting the database path.
//    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *docsPath = [paths objectAtIndex:0];
//    NSString *dbPath = [docsPath stringByAppendingPathComponent:@"sqldb.sqlite"];
//    
//    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
//    [database open];
//    NSString *deleteQuery = @"DELETE FROM college WHERE ID = 2";
//    [database executeUpdate:deleteQuery];
//    [database close];
//}

//PREPAREFORSEGUE SEGUE INTEGRATION ON CLICK MOVE TO DETAILED PAGE
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
//    
//    if([segue.identifier isEqualToString:@"toNotesDetails"])
//    {
//        
//        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
//        
//        NSLog(@"pathindex==%@",indexPath);
//        NSLog(@"indexpathrow==%ld",(long)indexPath.row);
//        
//        NotesDetails *controller = (NotesDetails *)segue.destinationViewController;
//        
//        NSLog(@"indexpathrow==%ld",(long)indexPath.row);
//        controller.PK = @"1001";
//        // here you have passed the value //
//        
//    }
//    
//}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



@end
