//
//  CenterVC.h
//  NotesiOS
//
//  Created by vivek athilkar on 09/12/16.
//  Copyright © 2016 vivek athilkar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MMDrawerController.h>
#import "AppDelegate.h"
#import <SWRevealViewController.h>

@interface CenterVC : UIViewController

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControlOutlet;

- (IBAction)segmentControlAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *leftBarButton;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *addBarButton;

@property (weak, nonatomic) IBOutlet UIView *ContainerOne;
@property (weak, nonatomic) IBOutlet UIView *ContainerTwo;

@property (weak, nonatomic) IBOutlet UIView *ContainerThree;




@end
