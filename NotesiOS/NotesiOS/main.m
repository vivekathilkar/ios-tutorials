  //
//  main.m
//  NotesiOS
//
//  Created by vivek athilkar on 09/12/16.
//  Copyright © 2016 vivek athilkar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
