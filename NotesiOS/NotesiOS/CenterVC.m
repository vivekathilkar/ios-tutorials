//
//  CenterVC.m
//  NotesiOS
//
//  Created by vivek athilkar on 09/12/16.
//  Copyright © 2016 vivek athilkar. All rights reserved.
//

#import "CenterVC.h"

@implementation CenterVC

-(void)viewDidLoad
{
    [super viewDidLoad];
    [self setTitle:@"Tutorials"];
    
//    AppDelegate *app = [[UIApplication sharedApplication]delegate];
//    app.window.rootViewController = app.drawerControllr;
}

-(void)viewDidAppear:(BOOL)animated

{
    [super viewDidAppear:YES];
    
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.leftBarButton setTarget: self.revealViewController];
        [self.leftBarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
}

- (IBAction)leftBarButton:(id)sender
{
    
    AppDelegate *app = [[UIApplication sharedApplication]delegate];
    [app.drawerControllr toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
    
}

- (IBAction)rightBarButton:(id)sender {
    
    AppDelegate *app = [[UIApplication sharedApplication]delegate];
    [app.drawerControllr toggleDrawerSide:MMDrawerSideRight animated:YES completion:nil];
}
- (IBAction)segmentControlAction:(id)sender {
    
    switch (((UISegmentedControl *)sender).selectedSegmentIndex)
    {
        case 0:
            self.ContainerOne.alpha = 1;
            self.ContainerTwo.alpha = 0;
            self.ContainerThree.alpha = 0;

            self.title = @"Tutorials";
            [self.addBarButton setEnabled:NO];
            [self.addBarButton setTintColor:[UIColor whiteColor]];
//            [self.navigationItem setRightBarButtonItem:_addBarButton animated:NO];

//            self.navigationController.navigationItem.rightBarButtonItem = _addBarButton;
//            [self.addButton setEnabled:NO];
//            [self.addButton setTintColor: [UIColor clearColor]];

            break;
            
        case 1:
            self.ContainerOne.alpha = 0;
            self.ContainerTwo.alpha = 1;
            self.ContainerThree.alpha = 0;

            self.title = @"Quiz";
//            [self.addBarButton setEnabled:NO];
//            [self.addButton setTintColor: [UIColor clearColor]];
            
            [self.addBarButton setEnabled:YES];
            [self.addBarButton setTintColor:[UIColor colorWithRed:0/255 green:76/255 blue:114/255 alpha:0]];
//            [self.navigationItem setRightBarButtonItem:nil animated:NO];
            
            break;
            
            case 2:
            self.ContainerOne.alpha = 0;
            self.ContainerTwo.alpha = 0;
            self.ContainerThree.alpha = 1;

            self.title = @"Interview";
            //            [self.addBarButton setEnabled:NO];
            //            [self.addButton setTintColor: [UIColor clearColor]];
            
            [self.addBarButton setEnabled:NO];
            [self.addBarButton setTintColor:[UIColor colorWithRed:0/255 green:76/255 blue:114/255 alpha:0]];
            //            [self.navigationItem setRightBarButtonItem:nil animated:NO];
            
            break;

            
        default:
            break;
    }

}



@end
