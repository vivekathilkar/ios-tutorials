//
//  NotesDetails.h
//  NotesiOS
//
//  Created by vivek athilkar on 09/12/16.
//  Copyright © 2016 vivek athilkar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FMDatabase.h>
#import <DLRadioButton.h>


@interface NotesDetails : UIViewController <UIWebViewDelegate>
{
    int value;
    NSMutableAttributedString * string;
}

@property(nonatomic,strong) NSString *PK;
//@property (weak, nonatomic) IBOutlet UITextView *noteDescrptionText;

//- (IBAction)bookmarkNote:(id)sender;

@property (nonatomic,assign) NSString *rowName;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;




@end
