//
//  CustomCell.m
//  NotesiOS
//
//  Created by vivek athilkar on 18/12/16.
//  Copyright © 2016 vivek athilkar. All rights reserved.
//

#import "CustomCell.h"

@implementation CustomCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)addConstrain
{
    _like.hidden = TRUE;
    _likeTopconstraint.constant = 8;
}

-(void)addConstrainaa
{
    _like.hidden = FALSE;
    _likeTopconstraint.constant = 0;
}

@end
