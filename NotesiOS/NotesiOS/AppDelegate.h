//
//  AppDelegate.h
//  NotesiOS
//
//  Created by vivek athilkar on 09/12/16.
//  Copyright © 2016 vivek athilkar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <MMDrawerController.h>
#import "Registration.h"
#import "CenterVC.h"


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (strong, nonatomic) MMDrawerController *drawerControllr;
@property (strong, nonatomic) Registration *Registration;
@property (strong, nonatomic) Registration *centerVC;




- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;


@end

