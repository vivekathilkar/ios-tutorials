//
//  Question.m
//  NotesiOS
//
//  Created by vivek athilkar on 16/12/16.
//  Copyright © 2016 vivek athilkar. All rights reserved.
//

#import "Question.h"
#import "optionsController.h"

static int gx = 0;
static int gx2 = 0;
static int gx3 = 0;
static int gx4 = 0;
static int gx5 = 0;


@interface Question ()

{
    NSTimer *timer;
    int currMinute;
    int currSeconds;
    NSMutableArray *keys;
    NSMutableArray *keysLevel2;
    NSMutableArray *keysLevel3;
    NSMutableArray *keysLevel4;
    NSMutableArray *keysLevel5;
    int questionArrayIndex;
    
}

@property (weak, nonatomic) IBOutlet UILabel *optionOne;
@property (weak, nonatomic) IBOutlet UILabel *optionTwo;
@property (weak, nonatomic) IBOutlet UILabel *optionThree;
@property (weak, nonatomic) IBOutlet UILabel *optionFour;

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UILabel *questionlabel;
@property (assign,nonatomic) NSInteger maxAllowedQuestions;
@property (assign,nonatomic) NSInteger currentQuestionIndex;
@property (nonatomic, strong) NSArray *questions;
@property (weak, nonatomic) IBOutlet UILabel *scorelabel;

//RADIO BUTTON
@property (weak, nonatomic) IBOutlet DLRadioButton *buttonFour;
@property (weak, nonatomic) IBOutlet DLRadioButton *buttonThree;
@property (weak, nonatomic) IBOutlet DLRadioButton *buttonTwo;
@property (weak, nonatomic) IBOutlet DLRadioButton *buttonOne;

@end

@implementation Question
@synthesize levelButtonTag,str,questionDetail;


//SINGLETON CLASS IMPLEMENTATION

static Question *thePlayer = nil;

+(Question *)thePlayer
{
    if(!thePlayer)
    {
        thePlayer = [[super allocWithZone:nil]init];
        
    }
    return thePlayer;
}

+(id)allocWithZone:(NSZone *)zone
{
    return [self thePlayer];
}

-(id)init
{
    self = [super init];
    if(self)
    {
        _scorecopy = _score;
    }
    
    return self;
}

+(void)resetSharedInstance
{
    thePlayer = nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"%ld",(long)gx);
    questionArrayIndex = 0;
    
    [[_nextButton layer]setBorderWidth:2.0f];
    [[_nextButton layer] setBorderColor:[UIColor darkGrayColor].CGColor];
    keys = [[NSMutableArray alloc]initWithObjects:@"key10",@"key11",@"key12", nil];
    keysLevel2 = [[NSMutableArray alloc]initWithObjects:@"key13",@"key14",@"key15", nil];
    keysLevel3 = [[NSMutableArray alloc]initWithObjects:@"key16",@"key17",@"key18", nil];
    keysLevel4 = [[NSMutableArray alloc]initWithObjects:@"key19",@"key20",@"key21", nil];
    keysLevel5 = [[NSMutableArray alloc]initWithObjects:@"key22",@"key23",@"key24", nil];
//   self.questionAttempted = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"value1", @"key1", @"value2", @"key2", nil];
//    [_questionAttempted setObject:@"one" forKey:@"Key1"];
//    [_questionAttempted setObject:@"two" forKey:@"Key2"];
//    [_questionAttempted setObject:@"three" forKey:@"Key3"];
    
//    NSLog(@"%@",[_questionAttempted allValues]);
//    NSLog(@"%@",[_questionAttempted objectForKey:@"key2"]);
//    NSLog(@"%@",_questionAttempted[@"key2"]);
    
    for(NSString *key in [_questionAttempted allKeys]) {
        NSLog(@"%@",[_questionAttempted objectForKey:key]);
    }
    
    _nextButton.enabled = TRUE;
    _questionlabel.layer.borderColor = [UIColor grayColor].CGColor;
    _questionlabel.layer.borderWidth = 1.0;
    _questionlabel.numberOfLines = 0;
    [_questionlabel sizeToFit];
    _questionlabel.layer.cornerRadius = 7;
//    _option1.clipsToBounds = YES;

    _scorecopy = _score;
    
    // Do any additional setup after loading the view.
    [_scrollView setShowsVerticalScrollIndicator:NO];

    _optionOne.layer.cornerRadius = 3; // this value vary as per your desire
    _optionOne.clipsToBounds = YES;
    
    _optionTwo.layer.cornerRadius = 3; // this value vary as per your desire
    _optionTwo.clipsToBounds = YES;
    
    _optionThree.layer.cornerRadius = 3; // this value vary as per your desire
    _optionThree.clipsToBounds = YES;
    
    _optionFour.layer.cornerRadius = 3; // this value vary as per your desire
    _optionFour.clipsToBounds = YES;
    
    self.currentQuestionIndex = 0;
    
    [_timeLabel setText:@"Time : 0:59"];
    [_timeLabel setTextColor:[UIColor redColor]];
    
    if((long)levelButtonTag==1)
    {
        [self setTitle:@"Level 1"];
        [self start];
        self.maxAllowedQuestions = 3;
        NSString *plistPath = [[NSBundle mainBundle]pathForResource:@"Questions" ofType:@"plist"];
        self.questions = [NSArray arrayWithContentsOfFile:plistPath];
        [self showNextQuestion:&(_maxAllowedQuestions) questionArray:_questions];
    }
    if((long)levelButtonTag==2)
    {
        [self setTitle:@"Level 2"];
        [self start];
        self.maxAllowedQuestions = 3;
        NSString *plistPath = [[NSBundle mainBundle]pathForResource:@"QueLevel2" ofType:@"plist"];
        self.questions = [NSArray arrayWithContentsOfFile:plistPath];
        [self showNextQuestion:&(_maxAllowedQuestions) questionArray:_questions];
    }
    
    if((long)levelButtonTag==3)
    {
        [self setTitle:@"Level 3"];
        [self start];
        self.maxAllowedQuestions = 3;
        NSString *plistPath = [[NSBundle mainBundle]pathForResource:@"QueLevel3" ofType:@"plist"];
        self.questions = [NSArray arrayWithContentsOfFile:plistPath];
        [self showNextQuestion:&(_maxAllowedQuestions) questionArray:_questions];
    }
    if((long)levelButtonTag==4)
    {
        [self setTitle:@"Level 4"];
        [self start];
        self.maxAllowedQuestions = 3;
        NSString *plistPath = [[NSBundle mainBundle]pathForResource:@"QueLevel4" ofType:@"plist"];
        self.questions = [NSArray arrayWithContentsOfFile:plistPath];
        [self showNextQuestion:&(_maxAllowedQuestions) questionArray:_questions];
    }
    if((long)levelButtonTag==5)
    {
        [self setTitle:@"Level 5"];
        [self start];
        self.maxAllowedQuestions = 3;
        NSString *plistPath = [[NSBundle mainBundle]pathForResource:@"QueLevel5" ofType:@"plist"];
        self.questions = [NSArray arrayWithContentsOfFile:plistPath];
        [self showNextQuestion:&(_maxAllowedQuestions) questionArray:_questions];
    }
    
    
}

//TIMER
-(void)start
{
    currMinute = 0;
    currSeconds = 10;
    timer=[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerFired) userInfo:nil repeats:YES];
    
}
-(void)timerFired
{
    if((currMinute>0 || currSeconds>=0) && currMinute>=0)
    {
        if(currSeconds==0)
        {
            currMinute-=1;
            currSeconds=59;
        }
        else if(currSeconds>0)
        {
            currSeconds-=1;
        }
        if(currMinute>-1)
            [_timeLabel setText:[NSString stringWithFormat:@"%@%d%@%02d",@"Time : ",currMinute,@":",currSeconds]];
    }
    else
    {
        
        [timer invalidate];
//        [self performSegueWithIdentifier:@"nextsegue" sender:self];
        [self nextQuestion];
    }
}

-(void)nextQuestion
{
    [self start];
    self.currentQuestionIndex++;
    [self showNextQuestion:&(_maxAllowedQuestions) questionArray:_questions];
}

-(void)showNextQuestion :(NSInteger *)maxAllowedQuestions questionArray:(NSArray *)questionArray
{
      if (self.currentQuestionIndex + 1 > self.maxAllowedQuestions) {
          
          NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
          
          switch(_score)
          {
              case 1:
                  if([prefs integerForKey:@"firstlevelScore"]  == _maxAllowedQuestions)
                  {
                      
                      [self launchDialog:@"!!!You Won!!!" messages:@"Go For Next Level"];
                  }
                  
                  else{
                      
                      [self launchDialog:@"!!! Failed !!!" messages:@"Try again to solve next level"];
                  }
                  break;
              case 2:
                  if([prefs integerForKey:@"secondlevelScore"]  == _maxAllowedQuestions)
                  {
                      [self launchDialog:@"!!!You Won!!!" messages:@"Go For Next Level"];
                  }
                  
                  else{
                      
                      [self launchDialog:@"!!! Failed !!!" messages:@"Try again to solve next level"];
                  }
                  break;
              case 3:
                  if([prefs integerForKey:@"thirdlevelScore"] == _maxAllowedQuestions)
                  {
                      [self launchDialog:@"!!!You Won!!!" messages:@"Go For Next Level"];
                  }
                  
                  else{
                      
                      [self launchDialog:@"!!! Failed !!!" messages:@"Try again to solve next level"];
                  }
                  break;
              case 4:
                  if([prefs integerForKey:@"fourthlevelScore"]  == _maxAllowedQuestions)
                  {
                      [self launchDialog:@"!!!You Won!!!" messages:@"Go For Next Level"];
                  }
                  
                  else{
                      
                      [self launchDialog:@"!!! Failed !!!" messages:@"Try again to solve next level"];
                  }
                  break;
              case 5:
                  if([prefs integerForKey:@"fifthlevelScore"] == _maxAllowedQuestions)
                  {
                      [self launchDialog:@"!!!You Won!!!" messages:@"Go For Next Level"];
                  }
                  
                  else{
                      
                      [self launchDialog:@"!!! Failed !!!" messages:@"Try again to solve next level"];
                  }
                  break;
          }

          _nextButton.enabled = FALSE;
          [timer invalidate];
          return;
      }
    
        questionDetail = questionArray[self.currentQuestionIndex];
        self.questionlabel.text = questionDetail[@"question"];
    
    switch (_score) {
        case 1:
        {
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            if([prefs boolForKey:[keys objectAtIndex:questionArrayIndex]] == TRUE)
            {
                int k = [(NSNumber *)self.questions[self.currentQuestionIndex][@"correctAnswer"] intValue];
                if(k == 1)
                {
                    _buttonOne.enabled = TRUE;
                    _buttonOne.selected = TRUE;
                    _buttonTwo.enabled = FALSE;
                    _buttonThree.enabled = FALSE;
                    _buttonFour.enabled = FALSE;
                }
                if(k == 2)
                {
                    _buttonOne.enabled = FALSE;
                    _buttonTwo.enabled = TRUE;
                    _buttonTwo.selected = TRUE;
                    _buttonThree.enabled = FALSE;
                    _buttonFour.enabled = FALSE;
                    
                }
                if(k == 3)
                {
                    _buttonOne.enabled = FALSE;
                    _buttonTwo.enabled = FALSE;
                    _buttonThree.enabled = TRUE;
                    _buttonThree.selected = TRUE;
                    _buttonFour.enabled = FALSE;
                }
                if(k == 4)
                {
                    _buttonOne.enabled = FALSE;
                    _buttonTwo.enabled = FALSE;
                    _buttonThree.enabled = FALSE;
                    _buttonFour.enabled = TRUE;
                    _buttonFour.selected = TRUE;
                }
                
            }
            
            else
            {
                _buttonOne.enabled = TRUE;
                _buttonTwo.enabled = TRUE;
                _buttonThree.enabled = TRUE;
                _buttonFour.enabled = TRUE;
            }
            
            break;
        }
        case 2:
        {
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            if([prefs boolForKey:[keysLevel2 objectAtIndex:questionArrayIndex]] == TRUE)
            {
                int k = [(NSNumber *)self.questions[self.currentQuestionIndex][@"correctAnswer"] intValue];
                if(k == 1)
                {
                    _buttonOne.enabled = TRUE;
                    _buttonOne.selected = TRUE;
                    _buttonTwo.enabled = FALSE;
                    _buttonThree.enabled = FALSE;
                    _buttonFour.enabled = FALSE;
                }
                if(k == 2)
                {
                    _buttonOne.enabled = FALSE;
                    _buttonTwo.enabled = TRUE;
                    _buttonTwo.selected = TRUE;
                    _buttonThree.enabled = FALSE;
                    _buttonFour.enabled = FALSE;
                    
                }
                if(k == 3)
                {
                    _buttonOne.enabled = FALSE;
                    _buttonTwo.enabled = FALSE;
                    _buttonThree.enabled = TRUE;
                    _buttonThree.selected = TRUE;
                    _buttonFour.enabled = FALSE;
                }
                if(k == 4)
                {
                    _buttonOne.enabled = FALSE;
                    _buttonTwo.enabled = FALSE;
                    _buttonThree.enabled = FALSE;
                    _buttonFour.enabled = TRUE;
                    _buttonFour.selected = TRUE;
                }
                
            }
            
            else
            {
                _buttonOne.enabled = TRUE;
                _buttonTwo.enabled = TRUE;
                _buttonThree.enabled = TRUE;
                _buttonFour.enabled = TRUE;
            }
            
            break;
        }
            
        case 3:
        {
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            if([prefs boolForKey:[keysLevel3 objectAtIndex:questionArrayIndex]] == TRUE)
            {
                int k = [(NSNumber *)self.questions[self.currentQuestionIndex][@"correctAnswer"] intValue];
                if(k == 1)
                {
                    _buttonOne.enabled = TRUE;
                    _buttonOne.selected = TRUE;
                    _buttonTwo.enabled = FALSE;
                    _buttonThree.enabled = FALSE;
                    _buttonFour.enabled = FALSE;
                }
                if(k == 2)
                {
                    _buttonOne.enabled = FALSE;
                    _buttonTwo.enabled = TRUE;
                    _buttonTwo.selected = TRUE;
                    _buttonThree.enabled = FALSE;
                    _buttonFour.enabled = FALSE;
                    
                }
                if(k == 3)
                {
                    _buttonOne.enabled = FALSE;
                    _buttonTwo.enabled = FALSE;
                    _buttonThree.enabled = TRUE;
                    _buttonThree.selected = TRUE;
                    _buttonFour.enabled = FALSE;
                }
                if(k == 4)
                {
                    _buttonOne.enabled = FALSE;
                    _buttonTwo.enabled = FALSE;
                    _buttonThree.enabled = FALSE;
                    _buttonFour.enabled = TRUE;
                    _buttonFour.selected = TRUE;
                }
                
            }
            
            else
            {
                _buttonOne.enabled = TRUE;
                _buttonTwo.enabled = TRUE;
                _buttonThree.enabled = TRUE;
                _buttonFour.enabled = TRUE;
            }
            
            break;
        }

        case 4:
        {
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            if([prefs boolForKey:[keysLevel4 objectAtIndex:questionArrayIndex]] == TRUE)
            {
                int k = [(NSNumber *)self.questions[self.currentQuestionIndex][@"correctAnswer"] intValue];
                if(k == 1)
                {
                    _buttonOne.enabled = TRUE;
                    _buttonOne.selected = TRUE;
                    _buttonTwo.enabled = FALSE;
                    _buttonThree.enabled = FALSE;
                    _buttonFour.enabled = FALSE;
                }
                if(k == 2)
                {
                    _buttonOne.enabled = FALSE;
                    _buttonTwo.enabled = TRUE;
                    _buttonTwo.selected = TRUE;
                    _buttonThree.enabled = FALSE;
                    _buttonFour.enabled = FALSE;
                    
                }
                if(k == 3)
                {
                    _buttonOne.enabled = FALSE;
                    _buttonTwo.enabled = FALSE;
                    _buttonThree.enabled = TRUE;
                    _buttonThree.selected = TRUE;
                    _buttonFour.enabled = FALSE;
                }
                if(k == 4)
                {
                    _buttonOne.enabled = FALSE;
                    _buttonTwo.enabled = FALSE;
                    _buttonThree.enabled = FALSE;
                    _buttonFour.enabled = TRUE;
                    _buttonFour.selected = TRUE;
                }
                
            }
            
            else
            {
                _buttonOne.enabled = TRUE;
                _buttonTwo.enabled = TRUE;
                _buttonThree.enabled = TRUE;
                _buttonFour.enabled = TRUE;
            }
            
            break;
        }

        case 5:
        {
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            if([prefs boolForKey:[keysLevel5 objectAtIndex:questionArrayIndex]] == TRUE)
            {
                int k = [(NSNumber *)self.questions[self.currentQuestionIndex][@"correctAnswer"] intValue];
                if(k == 1)
                {
                    _buttonOne.enabled = TRUE;
                    _buttonOne.selected = TRUE;
                    _buttonTwo.enabled = FALSE;
                    _buttonThree.enabled = FALSE;
                    _buttonFour.enabled = FALSE;
                }
                if(k == 2)
                {
                    _buttonOne.enabled = FALSE;
                    _buttonTwo.enabled = TRUE;
                    _buttonTwo.selected = TRUE;
                    _buttonThree.enabled = FALSE;
                    _buttonFour.enabled = FALSE;
                    
                }
                if(k == 3)
                {
                    _buttonOne.enabled = FALSE;
                    _buttonTwo.enabled = FALSE;
                    _buttonThree.enabled = TRUE;
                    _buttonThree.selected = TRUE;
                    _buttonFour.enabled = FALSE;
                }
                if(k == 4)
                {
                    _buttonOne.enabled = FALSE;
                    _buttonTwo.enabled = FALSE;
                    _buttonThree.enabled = FALSE;
                    _buttonFour.enabled = TRUE;
                    _buttonFour.selected = TRUE;
                }
                
            }
            
            else
            {
                _buttonOne.enabled = TRUE;
                _buttonTwo.enabled = TRUE;
                _buttonThree.enabled = TRUE;
                _buttonFour.enabled = TRUE;
            }
            
            break;
        }
            
            
    }

    for(int buttoncount = 1; buttoncount <= 4 ; buttoncount++)
        {
            switch(buttoncount)
            {
                case 1:
                    _optionOne.text = [questionDetail objectForKey:@"answer1"];
                    break;
                case 2:
                    _optionTwo.text = [questionDetail objectForKey:@"answer2"];
                    break;
                case 3:
                    _optionThree.text = [questionDetail objectForKey:@"answer3"];
                    break;
                case 4:
                    _optionFour.text = [questionDetail objectForKey:@"answer4"];
                    break;
            }
            
        }
}

-(void)buttonTag:(UIButton *)button
{
   if(button.tag == 1)
   {
    //PERFORM METHOD OPERATION FOR LEVEL 2
    NSLog(@"%ld",button.tag);
       _score = 1;
   }
    if(button.tag == 2)
    {
        //PERFORM METHOD OPERATION FOR LEVEL 2
        NSLog(@"%ld",button.tag);
        _score = 2;
    }
    if(button.tag == 3)
    {
        //PERFORM METHOD OPERATION FOR LEVEL 2
        NSLog(@"%ld",button.tag);
        _score = 3;
    }
    if(button.tag == 4)
    {
        //PERFORM METHOD OPERATION FOR LEVEL 2
        NSLog(@"%ld",button.tag);
        _score = 4;
    }
    if(button.tag == 5)
    {
        //PERFORM METHOD OPERATION FOR LEVEL 2
        NSLog(@"%ld",button.tag);
        _score = 5;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)logSelectedButton:(DLRadioButton *)radioButton {
    if (radioButton.isMultipleSelectionEnabled) {
        for (DLRadioButton *button in radioButton.selectedButtons) {
            NSLog(@"%@ is selected.\n", button.titleLabel.text);
                   }
    } else {
        NSLog(@"%@ is selected.\n", radioButton.selectedButton.titleLabel.text);
        a = radioButton.tag;
    }
}

- (IBAction)nextQuestion:(id)sender {

    int correctAnswer = [(NSNumber *)self.questions[self.currentQuestionIndex][@"correctAnswer"] intValue];
   
        if (a == correctAnswer) {
            
            switch (_score) {
                case 1:
                {
                    [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:[keys objectAtIndex:questionArrayIndex]];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                    NSLog(@"%d",[prefs boolForKey:[keys objectAtIndex:questionArrayIndex]]);
                    gx++;
                    self.scorelabel.text = [NSString stringWithFormat:@"%d",gx];
                    [[NSUserDefaults standardUserDefaults] setInteger:gx forKey:@"levelOneScore"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    break;
                }
                case 2:
                {

                    [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:[keysLevel2 objectAtIndex:questionArrayIndex]];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                    NSLog(@"%d",[prefs boolForKey:[keys objectAtIndex:questionArrayIndex]]);
                    gx2++;
                    self.scorelabel.text = [NSString stringWithFormat:@"%d",gx2];
                    [[NSUserDefaults standardUserDefaults] setInteger:gx forKey:@"levelTwoScore"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    break;
                }
                    
                case 3:
                {
                    [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:[keysLevel3 objectAtIndex:questionArrayIndex]];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                    NSLog(@"%d",[prefs boolForKey:[keysLevel3 objectAtIndex:questionArrayIndex]]);
                    gx3++;
                    if(gx == 3)
                   self.scorelabel.text = [NSString stringWithFormat:@"%d",gx3];
                    [[NSUserDefaults standardUserDefaults] setInteger:gx forKey:@"levelThreeScore"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    break;
                }
                case 4:
                {
                    
                    [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:[keysLevel4 objectAtIndex:questionArrayIndex]];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                    NSLog(@"%d",[prefs boolForKey:[keysLevel4 objectAtIndex:questionArrayIndex]]);
                    gx4++;
                  self.scorelabel.text = [NSString stringWithFormat:@"%d",gx4];
                    [[NSUserDefaults standardUserDefaults] setInteger:gx forKey:@"levelFourScore"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    break;
                }
                case 5:
                {
                    
                    [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:[keysLevel5 objectAtIndex:questionArrayIndex]];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                    NSLog(@"%d",[prefs boolForKey:[keysLevel5 objectAtIndex:questionArrayIndex]]);
                    gx5++;
                  self.scorelabel.text = [NSString stringWithFormat:@"%d",gx5];
                    [[NSUserDefaults standardUserDefaults] setInteger:gx forKey:@"levelFiveScore"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    break;
                }

              
            }
            
        if(gx == _maxAllowedQuestions&&levelButtonTag == 1)
        {
            [[NSUserDefaults standardUserDefaults] setInteger:gx  forKey:@"firstlevelScore"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
        if(gx2 == _maxAllowedQuestions&&levelButtonTag == 2)
        {
            
            [[NSUserDefaults standardUserDefaults] setInteger:gx2  forKey:@"secondlevelScore"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
        }
        
        if(gx3 == _maxAllowedQuestions&&levelButtonTag == 3)
        {
            
            [[NSUserDefaults standardUserDefaults] setInteger:gx3  forKey:@"thirdlevelScore"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
        }
        
        if(gx4 == _maxAllowedQuestions&&levelButtonTag == 4)
        {
            
            [[NSUserDefaults standardUserDefaults] setInteger:gx4  forKey:@"fourthlevelScore"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
        }
        
        if(gx5 == _maxAllowedQuestions&&levelButtonTag == 5)
        {
            
            [[NSUserDefaults standardUserDefaults] setInteger:gx5  forKey:@"fifthlevelScore"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
        }
    }
    self.currentQuestionIndex++;
    [timer invalidate];
    [self start];
    
    _buttonOne.selected = FALSE;
    _buttonTwo.selected = FALSE;
    _buttonThree.selected = FALSE;
    _buttonFour.selected = FALSE;

    questionArrayIndex++;
    [self showNextQuestion:&(_maxAllowedQuestions) questionArray:_questions];
    
    }
//CUSTOM ALERT VIEW
- (IBAction)launchDialog:(NSString *)title messages:(NSString *)message
{
    // Here we need to pass a full frame
    //    CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] initWithParentView:self.view];
    CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
    
    
    // Add some custom content to the alert view
    [alertView setContainerView:[self createDemoView:title messagess:message]];
    
    //    // Modify the parameters
    //    [alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"Close1", @"Close2", @"Close3", nil]];
    //    [alertView setDelegate:self];
    //
    //    // You may use a Block, rather than a delegate.
    //    [alertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
    //        NSLog(@"Block: Button at position %d is clicked on alertView %d.", buttonIndex, (int)[alertView tag]);
    //        [alertView close];
    //    }];
    //
    //    [alertView setUseMotionEffects:true];
    // And launch the dialog
    [alertView show];
}

- (void)customIOS7dialogButtonTouchUpInside: (CustomIOSAlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
    NSLog(@"Delegate: Button at position %d is clicked on alertView %d.", (int)buttonIndex, (int)[alertView tag]);
    [alertView close];
}

- (UIView *)createDemoView:(NSString *)title messagess:(NSString *)messag
{
    float textWidth = 260;
    
    float textMargin = 10;
    
    UILabel *titleLabel = [[UILabel alloc]init];
    
    titleLabel.font = [UIFont systemFontOfSize:25];
    
    titleLabel.lineBreakMode =NSLineBreakByWordWrapping;
    
    titleLabel.textAlignment = NSTextAlignmentCenter;
    
    titleLabel.text = title;
    titleLabel.textColor = [UIColor redColor];
    
    titleLabel.frame = CGRectMake(0, textMargin, textMargin * 2 + textWidth, 40);
    
    NSString *message =  messag;
    
    UIFont *textFont = [UIFont systemFontOfSize:20];
    
    NSMutableDictionary *attrs = [NSMutableDictionary dictionary];
    
    attrs[NSFontAttributeName] = textFont;
    
    CGSize maxSize = CGSizeMake(textWidth-textMargin*2, MAXFLOAT);
    
    CGSize size = [message boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attrs context:nil].size;
    
    UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(textMargin,CGRectGetMaxY(titleLabel.frame) + textMargin,textWidth, size.height)];
    
    textLabel.font = textFont;
    
    textLabel.textColor = [UIColor darkGrayColor];
    textLabel.textAlignment = NSTextAlignmentCenter;
    
    textLabel.backgroundColor = [UIColor clearColor];
    
    textLabel.lineBreakMode =NSLineBreakByWordWrapping;
    
    textLabel.numberOfLines =0;
    
    textLabel.text = message;
    
    UIView *demoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, textWidth + textMargin * 2,CGRectGetMaxY(textLabel.frame)+textMargin)];
    
    [demoView addSubview:titleLabel];
    
    [demoView addSubview:textLabel];
    
    NSLog(@"%@",demoView);
    
    return demoView;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    switch (_score) {
        case 1:
            _scorelabel.text = [NSString stringWithFormat:@"%ld",(long)[prefs integerForKey:@"levelOneScore"]];
            break;
        case 2:
            _scorelabel.text = [NSString stringWithFormat:@"%ld",(long)[prefs integerForKey:@"levelTwoScore"]];
            break;
        case 3:
            _scorelabel.text = [NSString stringWithFormat:@"%ld",(long)[prefs integerForKey:@"levelThreeScore"]];
            break;
        case 4:
            _scorelabel.text = [NSString stringWithFormat:@"%ld",(long)[prefs integerForKey:@"levelFourScore"]];
            break;
        case 5:
            _scorelabel.text = [NSString stringWithFormat:@"%ld",(long)[prefs integerForKey:@"levelFiveScore"]];
            break;
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [timer invalidate];
}

@end

