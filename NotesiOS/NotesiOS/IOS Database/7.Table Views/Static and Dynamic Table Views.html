<h2><span style="color: #008080;">Static and Dynamic Table Views</span></h2>
<p>An Overview of the Static Table Project</p>
<p>The preceding chapters worked through the implementation of an application designed to present the user with a list of cars. A dynamic table was used for this list since the number of table cells to be displayed was dependent upon the number of cars present in a data model. Selecting a car from the list triggered a segue to a second screen displaying details about the selected car. Regardless of the car selected, the detail screen always displays three items (the car&rsquo;s make and model together with a photograph). Whilst the previous example used a generic view controller and UIView for this purpose, clearly this is an ideal candidate for a static table view.</p>
<p>The remainder of this chapter will work through the implementation of a simple, stand alone application designed to demonstrate the implementation of a static table view using a storyboard. The finished application will essentially implement the car detail view from the previous chapter as a static table view.</p>
<p>Creating the Project</p>
<p>As with the previous project we will take advantage of the template provided by the Single View Application without actually using the single view provided by Xcode. Begin, therefore, by launching Xcode and creating a new single view application for iPhone named StaticTable with a matching class prefix.</p>
<p>As we will not be needing the provided view controller, select and delete the StaticTableViewController.h and StaticTableViewController.m files from the navigation panel. Also select the Main.storyboard file and within the storyboard editor, select the Static Table View Controller item before pressing the keyboard delete key to remove the scene.</p>
<p>Adding a Table View Controller</p>
<p>The example application is going to consist of a single table view controller. Adding this controller is a two step process involving the addition of both a new scene to the storyboard and also the files for a new subclass of UITableViewController.</p>
<p>With the storyboard editor still visible, drag and drop a Table View Controller object from the Object Library panel onto the storyboard canvas. With the scene added, select the File -&gt; New -&gt; File&hellip; menu option and select the option to add an Objective-C class. Click Next and on the options screen, name the new class StaticTableViewController and set the Subclass of menu to UITableViewController. Make sure that both the Targeted for iPad and With XIB for user interface options are disabled before clicking on Next to proceed with the creation process. Select a location for the new files before clicking on Create.</p>
<p>Once again, select the Main.storyboard file and select the Table View Controller scene so that it is highlighted in blue. Display the Identity Inspector panel (View -&gt; Utilities -&gt; Show Identity Inspector) and use the Class drop down menu to change the class from UITableViewController to StaticTableViewController.</p>
<p>Changing the Table View Content Type</p>
<p>By default, Xcode makes the assumption that the table view is to be dynamic. The most obvious indications of this are the presence of a Prototype Cell within the table view and the words &ldquo;Prototype Content&rdquo; in the view. Click within the grey area of the table view and display the Attributes Inspector panel. The Content attribute will currently be set to Dynamic Prototypes so use the menu to change the attribute to Static Cells. At this point the table view within the storyboard will change to display a static table containing three rows as illustrated in Figure 25-1:</p>
<p>Designing the Static Table</p>
<p>With a static table view added to the project, the full power of Interface Builder is available to us to design the layout and content of the table and cells. The first step is to change the table view style. With the table view selected in the storyboard and the Attributes Panel displayed, change the Style attribute to Grouped and the Sections attribute to 2. The table view should now consist of two group sections with three rows per section. For the purposes of this example we only need two rows in the top section so click on the third row to highlight it and press the delete key on the keyboard. Note that additional rows may be added if required by selecting the corresponding table view section and changing the Rows property.</p>
<p>In the case of the bottom section, only one row is required so Command-click on the bottom two rows and delete them from the view.</p>
<p>Select the top section by clicking on the shaded area immediately below the cells (note that sections may also be selected by clicking on the Table View Section item in the toolbar strip located across the top of the storyboard editor and also from the hierarchy list located in the Document Outline panel of the Xcode window). Within the Attributes Inspector change the Header property to Car Details. Repeat this step on the lower section to change the header to Car Photo.</p>
<p>Next, stretch the height of the single cell in the bottom section so that it fills the remainder of the table view such that the table view appears as illustrated in the following figure:</p>
<p>Adding Items to the Table Cells</p>
<p>Using the Object Library panel, drag and drop an Image View object into the cell in the bottom section and resize it to fill most of the available space. Also drag and drop labels into the two cells in the top section and set appropriate text properties so that the layout resembles that of Figure 25 3, stretching the right-hand labels until the margin guideline appears on the right hand edge of the view.</p>
<p>The user interface design of the table view is now complete so the next step is to create outlets to the labels and the image view. These, of course, need to be declared in the StaticTableViewController subclass.</p>
<p>Display the Assistant Editor either from the View -&gt; Assistant Editor -&gt; Show Assistant Editor menu option, or by selecting the middle button in the Editor cluster of buttons in the Xcode toolbar (the button displaying the bow tie and tuxedo image). Make sure that the editor is displaying the StaticTableViewController.h file, then click on the label located to the right of the &ldquo;Make&rdquo; label to select it, and then Ctrl-click and drag the resulting line to the body of the @interface section in the assistant editor panel as illustrated in Figure 25-4:</p>
<p>Upon releasing the line a dialog (Figure 25 5) will appear where information about the outlet needs to be declared. The default settings do not need to be changed so simply enter carMakeLabel into the Name field before clicking on the Connect button.</p>
<p>Repeat the above steps to establish connections between the second label and the image view, naming the outlets carModelLabel and carImageView respectively. Once the outlets are created and connected close the assistant window before proceeding.</p>
<p>Modifying the StaticTableViewController Class</p>
<p>When the StaticTableViewController class was created earlier in the chapter it was declared as being a subclass of UITableViewController. As a result of this selection, Xcode added a number of template methods intended to provide the basis for a data source for the table view. As previously discussed, however, static table views do not use a data source so these methods need to be removed.</p>
<p>Select the StaticTableViewController.m file and scroll down to the line that reads:</p>
<p>#pragma mark - Table view data source</p>
<p>To remove the data source from the class, delete the methods after this marker until you reach the @end marker.</p>
<p>All that remains to be implemented is the code to set the label and image outlets. Since this only needs to be performed once, the code may simply be added to the viewDidLoad method of the class:</p>
<p>- (void)viewDidLoad</p>
<p>{</p>
<p>&nbsp; &nbsp; [super viewDidLoad];</p>
<p>&nbsp;</p>
<p>&nbsp; &nbsp; _carMakeLabel.text = @"Volvo";</p>
<p>&nbsp; &nbsp; _carModelLabel.text = @"S60";</p>
<p>&nbsp; &nbsp; _carImageView.image = [UIImage imageNamed:@"volvo_s60.jpg"];</p>
<p>}</p>
<p>Finally, use a Finder window to locate the volvo_s60.jpg file downloaded in the previous chapter and drag and drop it onto the Supported Files section of the project navigator.</p>
<p>Building and Running the Application</p>
<p>The application is now complete and ready to run. Click on the Run button in the Xcode toolbar and wait for the application to load into the iOS Simulator. Once loaded the static table should appear:</p>
<p>&nbsp;An Overview of the Dynamic&nbsp;Table Project&nbsp;</p>
<p>Adding the TableView Controller to the Storyboard</p>
<p>From the perspective of the user, the entry point into this application will be a table view containing a list of cars, with each table view cell containing the vehicle make, model and corresponding image. As such, we will need to add a Table View Controller instance to the storyboard file. Select the MainStoryboard.storyboard file so that the canvas appears in the center of the Xcode window. From within the Object Library panel (accessible via the <em>View -&gt; Utilities -&gt; Show Object Library</em> menu option) drag a Table View Controller object and drop it onto the storyboard canvas as illustrated in Figure 18 1:</p>
<p>Within the storyboard we now have a table view controller instance. Within this instance is also a prototype table view cell that we will be able to configure to design the cells for our table. At the moment these are generic UITableViewCell and UITableViewController classes that do not give us much in the way of control within our application code. So that we can extend the functionality of these instances we need to declare them as being subclasses of UITableViewController and UITableViewCell respectively. Before doing so, however, we need to actually create those subclasses.</p>
<p>Creating the UITableViewController and UITableViewCell Subclasses</p>
<p>We will be declaring the Table View Controller instance within our storyboard as being a subclass of UITableViewController named CarTableViewController. At present, this subclass does not exist within our project so clearly we need to create it before proceeding. To achieve this, select the <em>File -&gt; New -&gt; New File&hellip;</em> menu option and in the resulting panel select the option to create a new UIViewController subclass. Click Next and on the subsequent screen, name the class CarTableViewController and change the Subclass of menu to UITableViewController. Make sure that the <em>Targeted for iPad</em> and <em>With XIB for user interface</em> options are both turned off and click Next followed by Create.</p>
<p>Within the Table View Controller added to the storyboard in the previous section, Xcode also added a prototype table cell. Later in this chapter we will add two labels and an image view object to this cell. In order to extend this class it is necessary to, once again, create a subclass. Perform this step by selecting the File -&gt; New -&gt; New File&hellip;. menu option. Within the new file dialog select Objective-C class and click Next. On the following screen, name the new class CarTableViewCell, change the Subclass of menu to UITableViewCell and proceed with the class creation.</p>
<p>Next, the items in the storyboard need to be configured to be instances of these subclasses. Begin by selecting the <em>MainStoryboard.storyboard</em> file and select the Navigation Controller scene so that it is highlighted in blue. Within the identity inspector panel (<em>View -&gt; Utilities -&gt; Show Identity Inspector</em>) use the <em>Class</em> drop down menu to change the class from <em>UITableViewController</em> to <em>CarTableViewController</em> as illustrated in Figure 18-2:</p>
<p>Similarly, select the prototype table cell within the table view controller storyboard scene and change the class from UITableViewCell to the new CarTableViewCell subclass. With the appropriate subclasses created and associated with the objects in the storyboard, the next step is to design the prototype cell.</p>
<p>Declaring the Cell Reuse Identifier</p>
<p>Later in the chapter some code will be added to the project to replicate instances of the prototype table cell. This will require that the cell be assigned a reuse identifier. With the storyboard still visible in Xcode, select the prototype table cell and display the Attributes Inspector. Within the inspector, change the <em>Identifier</em> field to carTableCell.</p>
<p>Designing a Storyboard UITableView Prototype Cell</p>
<p>Table Views are made up of multiple cells, each of which is actually either an instance of the UITableViewCell class or a subclass thereof. A useful feature of storyboarding allows the developer to visually construct the user interface elements that are to appear in the table cells and then replicate that cell at runtime. For the purposes of this example each table cell needs to display an image view and two labels which, in turn, will be connected to outlets that we will later declare in the CarTableViewCell subclass. Much like Interface Builder, components may be dragged from the Object Library panel and dropped onto a scene within the storyboard. Note, however, that this is only possible when the storyboard view is zoomed in. With this in mind, verify that the storyboard is zoomed in using the controls in the bottom right hand corner of the canvas and then drag and drop two Labels and an Image View object onto the prototype table cell. Resize and position the items so that the cell layout resembles that illustrated in Figure 18-4, making sure to stretch the label objects so that they extend toward the right hand edge of the cell.</p>
<p>Having configured the storyboard elements for the table view portion of the application it is time to begin modifying the table view and cell subclasses.</p>
<p>&nbsp;</p>
<p>Modifying the CarTableViewCell Class</p>
<p>Within the storyboard file two labels and an image view were added to the prototype cell which, in turn, has been declared as an instance of our new CarTableViewCell class. In order to manipulate these user interface objects from within our code we need to declare three outlets and then connect those outlets to the objects in the storyboard scene. Begin, therefore, by selecting the <em>CarTableViewCell.h</em> file and adding the three outlet properties as follows:</p>
<p>#import &lt;UIKit/UIKit.h&gt;</p>
<p>&nbsp;</p>
<p>@interface CarTableViewCell&nbsp;: UITableViewCell</p>
<p>@property (nonatomic, strong) IBOutlet UIImageView *carImage;</p>
<p>@property (nonatomic, strong) IBOutlet UILabel *makeLabel;</p>
<p>@property (nonatomic, strong) IBOutlet UILabel *modelLabel;</p>
<p>@end</p>
<p>Having declared the properties, synthesize access within the CarTableViewCell.m implementation file:</p>
<p>#import "CarTableViewCell.h"</p>
<p>@implementation CarTableViewCell</p>
<p>@synthesize makeLabel = _makeLabel;</p>
<p>@synthesize modelLabel = _modelLabel;</p>
<p>@synthesize carImage = _carImage;</p>
<p>@end</p>
<p>With the outlet properties declared the next step is to establish the connections to the user interface objects. Within the storyboard file Ctrl-click on the white background of the prototype table cell and drag the resulting blue line to the uppermost of the two labels as outlined in Figure 18-5:</p>
<p>Upon releasing the pointer, select makeLabel from the resulting menu to establish the outlet connection:</p>
<p>Repeat these steps to connect the modelLabel and carImage outlets to the second label and image view objects respectively.</p>
<p>Creating the Table View Datasource</p>
<p>Dynamic Table Views require a datasource to provide the data that will be displayed to the user within the cells. By default, Xcode has designated the CarTableViewController class as the datasource for the table view controller in the storyboard. It is within this class, therefore, that we can build a very simple data model for our application consisting of a number of arrays. The first step is to declare these as properties in the <em>CarTableViewController.h</em> file:</p>
<p>#import &lt;UIKit/UIKit.h&gt;</p>
<p>&nbsp;</p>
<p>@interface CarTableViewController&nbsp;: UITableViewController</p>
<p>&nbsp;</p>
<p>@property (nonatomic, strong) NSArray *carImages;</p>
<p>@property (nonatomic, strong) NSArray *carMakes;</p>
<p>@property (nonatomic, strong) NSArray *carModels;</p>
<p>@end</p>
<p>With the properties declared these need to be synthesized within the <em>CarTableViewController.m</em> file. In addition, the arrays need to be initialized with some data when the application has loaded, making the viewDidLoad: method an ideal location. Select the <em>CarTableViewController.m</em> file within the project navigator panel and modify it as outlined in the following code fragment. Since we will be working with CarTableViewCell instances within the code it is also necessary to import the <em>CarTableViewCell.h</em> file:</p>
<p>#import "CarTableViewController.h"</p>
<p>#import &ldquo;CarTableViewCell.h&rdquo;</p>
<p>@implementation CarTableViewController</p>
<p>@synthesize carMakes = _carMakes;&nbsp;</p>
<p>@synthesize carModels = _carModels;</p>
<p>@synthesize carImages = _carImages;</p>
<p>- (void)viewDidLoad</p>
<p>{</p>
<p>&nbsp; &nbsp; [super viewDidLoad];</p>
<p>&nbsp; &nbsp; self.carMakes = [[NSArray alloc]</p>
<p>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; initWithObjects:@"Chevy",</p>
<p>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; @"BMW",</p>
<p>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; @"Toyota",</p>
<p>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; @"Volvo",</p>
<p>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; @"Smart", nil];</p>
<p>&nbsp;</p>
<p>&nbsp; &nbsp; self.carModels = [[NSArray alloc]</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; initWithObjects:@"Volt",</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; @"Mini",</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; @"Venza",</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; @"S60",</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; @"Fortwo", nil];</p>
<p>&nbsp; &nbsp; self.carImages = [[NSArray alloc]</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; initWithObjects:@"chevy_volt.jpg",</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; @"mini_clubman.jpg",</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; @"toyota_venza.jpg",</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; @"volvo_s60.jpg",</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; @"smart_fortwo.jpg", nil];</p>
<p>}</p>
<p>For a class to act as the datasource for a table view controller a number of methods must be implemented. These methods will be called by the table view object in order to obtain both information about the table and also the table cell objects to display. When we created the CarTableViewController class we specified that it was to be a subclass of UITableViewController. As a result, Xcode created templates of these data source methods for us within the <em>CarTableViewController.m</em> file. To locate these template datasource methods, scroll down the file until the <em>#pragma mark &ndash; Table view data source</em> marker comes into view. The first template method, named numberOfSectionsInTableView: needs to return the number of sections in the table. For the purposes of this example we only need one section so will simply return a value of 1 (note also that the #warning line needs to be removed):</p>
<p>- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView</p>
<p>{</p>
<p>&nbsp; &nbsp; // Return the number of sections.</p>
<p>&nbsp; &nbsp; return 1;</p>
<p>}</p>
<p>The next method is required to return the number of rows to be displayed in the table. This is equivalent to the number of items in our carModels array so can be modified as follows:</p>
<p>- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section</p>
<p>{</p>
<p>&nbsp; &nbsp; // Return the number of rows in the section.</p>
<p>&nbsp; &nbsp; return [self.carModels count];</p>
<p>}</p>
<p>The above code calls the count method on the carModels array object to obtain the number of items in the array and returns that value to the table view.</p>
<p>The final datasource method that needs to be modified is cellForRowAtIndexPath:. Each time the table view controller needs a new cell to display it will call this method and pass through an index value indicating the row for which a cell object is required. It is the responsibility of this method to create a new instance of our CarTableViewCell class (unless an instance already exists for re-use) and extract the correct car make, model and image file name from the data arrays based on the index value passed through to the method. The code will then set those values on the appropriate outlets on the CarTableViewCell object. Begin by removing the template code from this method and then re-write the method so that as reads as follows:</p>
<p>- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath</p>
<p>{</p>
<p>&nbsp; &nbsp; static NSString *CellIdentifier = @"carTableCell";</p>
<p>&nbsp;</p>
<p>&nbsp; &nbsp; CarTableViewCell *cell = [tableView</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; dequeueReusableCellWithIdentifier:CellIdentifier];</p>
<p>&nbsp; &nbsp; if (cell == nil) {</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; cell = [[CarTableViewCell alloc]</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; initWithStyle:UITableViewCellStyleDefault&nbsp;</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; reuseIdentifier:CellIdentifier];</p>
<p>&nbsp; &nbsp; }</p>
<p>&nbsp; &nbsp; // Configure the cell...</p>
<p>&nbsp; &nbsp; cell.makeLabel.text = [self.carMakes&nbsp;</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; objectAtIndex: [indexPath row]];</p>
<p>&nbsp;</p>
<p>&nbsp; &nbsp; cell.modelLabel.text = [self.carModels&nbsp;</p>
<p>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; objectAtIndex:[indexPath row]];</p>
<p>&nbsp;</p>
<p>&nbsp; &nbsp; UIImage *carPhoto = [UIImage imageNamed:&nbsp;</p>
<p>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; [self.carImages objectAtIndex: [indexPath row]]];</p>
<p>&nbsp;</p>
<p>&nbsp; &nbsp; cell.carImage.image = carPhoto;</p>
<p>&nbsp;</p>
<p>&nbsp; &nbsp; return cell;</p>
<p>}</p>
<p>Before proceeding with this tutorial we need to take some time to deconstruct this code and explain what is actually happening. The code begins by creating a string that represents the reuse identifier that was assigned to the CarTableViewCell class within the storyboard (in this instance the identifier was set to carTableCell):</p>
<p>static NSString *CellIdentifier = @"carTableCell";</p>
<p>Next, the following lines of code are executed:</p>
<p>&nbsp; &nbsp; CarTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];</p>
<p>&nbsp; &nbsp; if (cell == nil) {</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; cell = [[CarTableViewCell alloc]&nbsp;</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; initWithStyle:UITableViewCellStyleDefault&nbsp; &nbsp;</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; reuseIdentifier:CellIdentifier];</p>
<p>&nbsp; &nbsp; }</p>
<p>This code calls the dequeueReusableCellWithIdentifier: method of the table view object to find out if an existing cell is available for re-use as the new row. If no cell is available then a new instance of the CarTableViewCell class is created ready for use. Having either created a new cell, or obtained an existing reusable cell the code simply uses the outlets previously added to the CarTableViewCell class to set the labels with the car make and model. The code then creates a new UIImage object configured with the image of the current car and assigns it to the image view outlet. Finally, the method returns the cell object to the table view:</p>
<p>// Configure the cell...</p>
<p>&nbsp; &nbsp; cell.makeLabel.text = [self.carMakes objectAtIndex: [indexPath row]];</p>
<p>&nbsp;</p>
<p>&nbsp; &nbsp; cell.modelLabel.text = [self.carModels&nbsp;</p>
<p>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; objectAtIndex:[indexPath row]];</p>
<p>&nbsp;</p>
<p>&nbsp; &nbsp; UIImage *carPhoto = [UIImage imageNamed:&nbsp;</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; [self.carImages objectAtIndex: [indexPath row]]];</p>
<p>&nbsp;</p>
<p>&nbsp; &nbsp; cell.carImage.image = carPhoto;</p>
<p>&nbsp;</p>
<p>&nbsp; &nbsp; return cell;</p>
<p>Downloading and Adding the Image Files</p>
<p>Before a test run of the application can be performed the image files referenced in the code need to be added to the project. An archive containing the images may be downloaded from the following URL:</p>
<p><a href="http://www.ebookfrenzy.com/code/carImages.zip">http://www.ebookfrenzy.com/code/carImages.zip</a></p>
<p>Once the file has been downloaded, unzip the files and then drag and drop them from a Finder window onto the Supporting Files category of the Xcode project navigator panel.</p>
<p>Compiling and Running the Application</p>
<p>Now that the storyboard work and code modifications are complete the final step in this chapter is to run the application by clicking on the <em>Run</em> button located in the Xcode toolbar. Once the code has compiled the application will launch and execute within an iOS Simulator session as illustrated in Figure 18-7.</p>
<p>Clearly the table view has been populated with multiple instances of our prototype table view cell, each of which has been customized through outlets to display different car information and photos. The next step, which will be outlined in the next chapter entitled <a href="http://www.techotopia.com/index.php/Implementing_TableView_Navigation_using_Xcode_Storyboards">Implementing TableView Navigation using Xcode Storyboards</a> will be to use the storyboard to add navigation capabilities to the application so that selecting a row from the table results in a detail scene appearing to the user.</p>