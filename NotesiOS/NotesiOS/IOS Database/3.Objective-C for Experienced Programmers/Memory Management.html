<h2><span style="color: #008080;"><strong>Memory Management</strong></span></h2>
<p>Memory on the iPhone is limited. Although each new generation of hardware has increased the amount of physical memory in the device, applications are also taking more memory, and with the introduction of background apps in iOS 4, memory remains a resource to be conserved. iOS automatically quits applications that use too much memory; therefore it&rsquo;s important to be aware of your memory usage and avoid leaking, which will increase your memory usage, or the equally annoying early release of elements, which will cause your app to crash. These issues are handled by the system in a garbage-collected environment, where the system will automatically release memory as needed, but this adds an overhead and is not available on iOS.</p>
<p>On a similar topic, note that iOS does not have a paging system&mdash;data cannot be paged out to disk as needed. You can read and write data to disk, but this is not recommended for any object&mdash;only for data files, and even then sparingly, because disk access is inevitably slower than memory access.</p>
<p><span style="color: #3366ff;"><strong>Reference Counting</strong></span></p>
<p>Cocoa&rsquo;s memory management scheme is based off a reference counting system. It&rsquo;s a simple concept&mdash;every object has a value, known as a reference count. When the object is created, the count is set to 1. Certain actions will increment or decrement this count, and when the count hits zero, the object is immediately freed from memory, at which point you cannot access it again.</p>
<p><span style="color: #3366ff;"><strong>Messages</strong></span></p>
<p>Reference counting principally involves three methods&mdash;retain, release, and dealloc. The first two change the reference count; the latter frees up the memory of the objects. You generally override the dealloc method, as we have been doing in our classes, to free up the memory occupied by the class&rsquo;s instance variables (usually by calling their dealloc method). Finally, the method always includes a call to super&lsquo;s dealloc method, which invariably calls NSObject&lsquo;s dealloc method. It is this specific method that frees up the memory; if you neglect to call this implementation, you get a compiler warning. Also note that you should always override this method, but you should never call it specifically&mdash;the system will call that method for you as necessary.</p>
<p>The retain method increments the retain count by one. It is inherited by all classes, and so can be called on any object, any number of times, at any time. The release method decrements the retain count by one.</p>
<p>Keeping track of the retain count requires some diligence on the part of the programmer. If you under-release an object, the memory will be leaked if you over-release, you crash.</p>
<p><span style="color: #3366ff;"><strong>Memory Management Rules</strong></span></p>
<p>The rules are simple, really.</p>
<ul>
<li>If the method name has init, copy, or new anywhere in the method name (such as initWithName:, mutableCopy, or newCar), the method implies that the object will have been retained, and it is your responsibility to release that object later This also means that you&rsquo;ll need to store that object in a variable, to be able to release it again.<br /> NSMutableArray *ma = [someArray mutableCopy]; myMutableArray = ma; [ma release];</li>
<li>Any other method name, if the method returns an object, will be autoreleased.</li>
</ul>
<p>When you create a property, you can specify certain parameters that the setters follow. Of note is the retain and copy parameters. Most object properties are declared as retain, which generates a setter like so:</p>
<ol>
<li>-(void) setText:(NSString *)textValue {</li>
<li>&nbsp; &nbsp; if (![textValue isEqualToString: text]) {</li>
<li>&nbsp; &nbsp; &nbsp; &nbsp; [textValue retain];</li>
<li>&nbsp; &nbsp; &nbsp; &nbsp; [text release];</li>
<li>&nbsp; &nbsp; &nbsp; &nbsp; text = textValue;</li>
<li>&nbsp; &nbsp; }</li>
<li>}</li>
</ol>
<p><span style="color: #3366ff;"><strong>Autorelease</strong></span></p>
<p>The autorelease pool is a medium between directly managing memory, and garbage collection. A lot of methods return autoreleased objects&mdash;these objects are added to an autorelease pool, which is created in every program. When the drain method is sent to the pool, all of the objects within are sent a release method. This can be useful in a tight loop:</p>
<ol>
<li>NSAutoreleasePool *tempPool;</li>
<li>for (i = 0; i &lt; n; i++) {</li>
<li>&nbsp; &nbsp; &nbsp;tempPool = [[NSAutoreleasePool alloc] init];</li>
<li>&nbsp; &nbsp; &nbsp;// Create lots of temporary autoreleased objects that take a lot of memory</li>
<li>&nbsp; &nbsp; &nbsp;[tempPool drain];</li>
<li>}</li>
</ol>
<p>To autorelease an object, simply send the autorelease method.</p>
<h2><span id="Calling_Methods_and_Accessing_Instance_Data" class="mw-headline">Calling Methods and Accessing Instance Data</span></h2>
<p>Given the length of this chapter, now is probably a good time to recap what we have done so far. We have now created a new class called BankAccount. Within this new class we declared some instance variables to contain the bank account number and current balance together with some instance methods used to set, get and display these values. In the preceding section we covered the steps necessary to create and initialize an instance of our new class. The next step is to learn how to call the instance methods we built into our class.</p>
<p>The syntax for invoking methods is to place the object pointer variable name and method to be called in square brackets ([]). For example, to call the displayAccountInfo method on the instance of the class we created previously we would use the following syntax:</p>
<pre>[account1 displayAccountInfo];
</pre>
<p>When the method accepts a single argument, the method name is followed by a colon (:) followed by the value to be passed to the method. For example, to set the account number:</p>
<pre>[account1 setAccountNumber: 34543212];
</pre>
<p>In the case of methods taking multiple arguments (as is the case with our setAccount method) syntax similar to the following is employed:</p>
<pre>[account1 setAccount: 4543455 andBalance: 3010.10];
</pre>
<h3><span id="Objective-C_and_Dot_Notation" class="mw-headline" style="color: #0000ff;">Objective-C and Dot Notation</span></h3>
<p>Those familiar with object oriented programming in Java, C++ or C# are probably reeling a little from the syntax used in Objective-C. They are probably thinking life was much easier when they could just use something called dot notation to set and get the values of instance variables. The good news is that one of the features introduced into version 2.0 of Objective-C is support for dot notation.</p>
<p>Dot notation involves accessing an instance variable by specifying a class instance followed by a dot followed in turn by the name of the instance variable or property to be accessed:</p>
<pre>classinstance.property
</pre>
<p>For example, to the get current value of our accountBalance instance variable:</p>
<pre>double balance1 = account1.accountBalance;
</pre>
<p>Dot notation can also be used to set values of instance properties:</p>
<pre>account1.accountBalance = 6789.98;
</pre>
<p>A key point to understand about dot notation is that it only works for instance variables for which synthesized accessor methods have been declared. If you attempt to use dot notation to access an instance variable for which no synthesized accessor is available the code will fail to compile with an error similar to:</p>
<pre>error: request for member 'accountBalance' in something not a structure or union
</pre>
<h3><span id="How_Variables_are_Stored" class="mw-headline" style="color: #0000ff;">How Variables are Stored</span></h3>
<p>When we declare a variable in Objective-C and assign a value to it we are essentially allocating a location in memory where that value is stored. Take, for example, the following variable declaration:</p>
<pre>int myvar = 10;
</pre>
<p>When the above code is executed, a block of memory large enough to hold an integer value is reserved in memory and the value of 10 is placed at that location. Whenever we reference this variable in code, we are actually using the variable value. For example, the following code adds the value of myvar (i.e. 10) to the constant value 20 to arrive at a result of 30.</p>
<pre>int result = 20 + myvar;
</pre>
<p>Similarly, when we pass a variable through as an argument to a method or function we are actually passing the value of the variable, not the variable itself. To better understand this concept, consider the following sample program:</p>
<pre>#import &lt;Foundation/Foundation.h&gt;

void myFunction(int i)
{
        i = i + 10;
}

int main (int argc, const char * argv[])
{
        NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];

        int myvar = 10;

        NSLog (@"Before call to function myvar =&nbsp;%i", myvar);
        myFunction (myvar);
        NSLog (@"After call to function myvar =&nbsp;%i", myvar);

        [pool drain];

        return 0;
}
</pre>
<p>The above program consists of a main function that declares our myvar variable and displays the current value. It then calls the function myFunction passing through the value of the myvar variable. The myFunction function adds 10 to the value it was passed as an argument and then returns to the main function where the value of myvar is once again displayed. When compiled and executed the following output is displayed:</p>
<pre>Before call to function myvar = 10
After call to function myvar = 10
</pre>
<p>Clearly, even though the value passed through to myFunction was increased by 20 the value of myvar remained unchanged. This is because what was passed through as an argument to myFunction was the value of myvar, not the myvar variable itself. Therefore, in myFunction we were simply working on a constant value of 10 that had absolutely no connection to the original myvar variable.</p>
<p>In order to be able to work on the actual variable in the function we need to use something called indirection.</p>