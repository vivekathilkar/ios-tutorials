<h2><span style="color: #008080;"><strong>Classes, Objects, and Methods</strong></span></h2>
<h3><span id="What_is_an_Object.3F" class="mw-headline" style="color: #0000ff;">What is an Object?</span></h3>
<p>Objects are self-contained modules of functionality that can be easily used, and re-used as the building blocks for a software application.</p>
<p>Objects consist of data variables and functions (called methods) that can be accessed and called on the object to perform tasks. These are collectively referred to as members.</p>
<h3><span id="What_is_a_Class.3F" class="mw-headline" style="color: #0000ff;">What is a Class?</span></h3>
<p>Much as a blueprint or architect's drawing defines what an item or a building will look like once it has been constructed, a class defines what an object will look like when it is created. It defines, for example, what the methods will do and what the member variables will be.</p>
<h3><span id="Declaring_an_Objective-C_Class_Interface" class="mw-headline" style="color: #0000ff;">Declaring an Objective-C Class Interface</span></h3>
<p>Before an object can be instantiated we first need to define the class 'blueprint' for the object. In this chapter we will create a Bank Account class to demonstrate the basic concepts of Objective-C object oriented programming.</p>
<p>An Objective-C class is defined in terms of an interface and an implementation. In the interface section of the definition we specify the base class from which the new class is derived and also define the members and methods that the class will contain. The syntax for the interface section of a class is as follows:</p>
<pre>@interface NewClassName: ParentClass {
    ClassMembers;	
}
ClassMethods;
@end
</pre>
<p>The ClassMembers section of the interface defines the variables that are to be contained within the class (also referred to as instance variables). These variables are declared in the same way that any other variable would be declared in Objective-C.</p>
<p>The ClassMethods section defines the methods that are available to be called on the class. These are essentially functions specific to the class that perform a particular operation when called upon.</p>
<p>To create an example outline interface section for our BankAccount class, we would use the following:</p>
<pre>@interface BankAccount: NSObject
{
}
@end
</pre>
<p>The parent class chosen above is the NSObject class. This is a standard base class provided with the Objective-C Foundation framework and is the class from which most new classes are derived. By deriving BankAccount from this parent class we inherit a range of additional methods used in creating, managing and destroying instances that we would otherwise have to write ourselves.<span id="ezoic-pub-ad-placeholder-143" class="ezoic-adpicker-ad" data-ezadpicker="143" data-ez-position-type="long_content"></span></p>
<p>Now that we have the outline syntax for our class, the next step is to add some instance variables to it.</p>
<h3><span id="Adding_Instance_Variables_to_a_Class" class="mw-headline" style="color: #0000ff;">Adding Instance Variables to a Class</span></h3>
<p>A key goal of object oriented programming is a concept referred to as data encapsulation. The idea behind data encapsulation is that data should be stored within classes and accessed only through methods defined in that class. Data encapsulated in a class are referred to as instance variables.</p>
<p>Instances of our BankAccount class will be required to store some data, specifically a bank account number and the balance currently held by the account. Instance variables are declared in the same way any other variables are declared in Objective-C. We can, therefore, add these variables as follows:</p>
<pre>@interface BankAccount: NSObject
{
        double accountBalance;
        long accountNumber;
}
@end
</pre>
<p>Having defined our instance variables, we can now move on to defining the methods of the class that will allow us to work with our instance variables while staying true to the data encapsulation model.</p>
<h3><span id="Define_Class_Methods" class="mw-headline" style="color: #0000ff;">Define Class Methods</span></h3>
<p>The methods of a class are essentially code routines that can be called upon to perform specific tasks within the context of an instance of that class.</p>
<p>Methods come in two different forms, class methods and instance methods. Class methods operate at the level of the class, such as creating a new instance of a class. Instance methods, on the other hand, operate only on the instance of a class (for example performing an arithmetic operation on two instance variables and returning the result). Class methods are preceded by a plus (+) sign in the declaration and instance methods are preceded by a minus (-) sign. If the method returns a result, the name of method must be preceded by the data type returned enclosed in parentheses. If a method does not return a result, then the method must be declared as void. If data needs to be passed through to the method (referred to as arguments), the method name is followed by a colon, the data type in parentheses and a name for the argument. For example, the declaration of a method to set the account number in our example might read as follows:</p>
<pre>-(void) setAccountNumber: (long) y;
</pre>
<p>The method is an instance method so it is preceded by the minus sign. It does not return a result so it is declared as (void). It takes an argument (the account number) of type long so we follow the accountNumber name with a colon (:) specify the argument type (long) and give the argument a name (in this case we simply use y).</p>
<p>The following method is intended to return the current value of the account number instance variable (which is of type long):</p>
<pre>-(long) getAccountNumber;
</pre>
<p>Methods may also be defined to accept more than one argument. For example to define a method that accepts both the account number and account balance we could declare it as follows:</p>
<pre>-(void) setAccount: (long) y andBalance: (double) x;
</pre>
<p>Now that we have an understanding of the structure of method declarations within the context of the class interface definition, we can extend our BankAccount class accordingly:</p>
<pre>@interface BankAccount: NSObject
{
        double accountBalance;
        long accountNumber;
}
-(void) setAccount: (long) y andBalance: (double) x;
-(void) setAccountBalance: (double) x;
-(double) getAccountBalance;
-(void) setAccountNumber: (long) y;
-(long) getAccountNumber;
-(void) displayAccountInfo;
@end
</pre>
<p>Having defined the interface, we can now move on to defining the implementation of our class.</p>
<div align="center">&nbsp;</div>
<h3><span id="Declaring_an_Objective-C_Class_Implementation" class="mw-headline" style="color: #0000ff;">Declaring an Objective-C Class Implementation</span></h3>
<p>The next step in creating a new class in Objective-C is to write the code for the methods we have already declared. This is performed in the @implementation section of the class definition. An outline implementation is structured as follows:</p>
<pre>@implementation NewClassName
    ClassMethods
@end
</pre>
<p>In order to implement the methods we declared in the @interface section, therefore, we need to write the following code:</p>
<pre>@implementation BankAccount

-(void) setAccount: (long) y andBalance: (double) x;
{
        accountBalance = x;
        accountNumber = y;
}
-(void) setAccountBalance: (double) x
{
        accountBalance = x;
}
-(double) getAccountBalance
{
        return accountBalance;
}
-(void) setAccountNumber: (long) y
{
        accountNumber = y;
}
-(long) getAccountNumber
{
        return accountNumber;
}
-(void) displayAccountInfo
{
        NSLog (@"Account Number&nbsp;%i has a balance of&nbsp;%f", accountNumber, accountBalance);
}
@end
</pre>
<p>We are now at the point where we can write some code to work with our new BankAccount class.</p>
<h3><span id="Declaring_and_Initializing_a_Class_Instance" class="mw-headline" style="color: #0000ff;">Declaring and Initializing a Class Instance</span></h3>
<p>So far all we have done is define the blueprint for our class. In order to do anything with this class, we need to create instances of it. The first step in this process is to declare a variable to store a pointer to the instance when it is created. We do this as follows:</p>
<pre>BankAccount *account1;
</pre>
<p>Having created a variable to store a reference to the class instance, we can now allocate memory in preparation for initializing the class:</p>
<pre>account1 = [BankAccount alloc];
</pre>
<p>In the above statement we are calling the alloc method of the BankAccount class (note that alloc is a class method inherited from the parent NSObject class, as opposed to an instance method created by us in the BankAccount class).</p>
<p>Having allocated memory for the class instance, the next step is to initialize the instance by calling the init instance method:</p>
<pre>account1 = [account1 init];
</pre>
<p>For the sake of economy of typing, the above three statements are frequently rolled into a single line of code as follows:</p>
<pre>BankAccount *account1 = [[BankAccount alloc] init];</pre>