<h2><span style="color: #008080;"><strong>Automatic Reference Counting (ARC)</strong></span></h2>
<p><strong>Automatic Reference Counting</strong> (<strong>ARC</strong>) is a memory-management implementation in the Clang compiler for the Objective-C and Swift programming languages. Like reference counting, it is based on release and retain messages with which objects can be marked for deallocation or retention at compile time, thereby allowing the developer to control the deallocation of objects and the system to deallocate them instantly when they are no longer needed. ARC absolves the developer from implementing these messages themselves, instead relying on the compiler to insert these messages in the object code automatically.</p>
<p>ARC differs from garbage collection in that there is no background process that deallocates the objects asynchronously at runtime.Unlike garbage collection, ARC does not handle reference cycles automatically. This means that as long as there are "strong" references to an object, it will not be deallocated. Strong cross-references can accordingly create deadlocks and memory leaks. It is up to the developer to break cycles by using weak references.</p>
<p>Apple Inc. deploys ARC in their operating systems, such as macOS (OS X) and iOS. Limited support (ARCLite) has been available since Mac OS X Snow Leopard and iOS 4, with complete support following in Mac OS X Lion and iOS 5. Garbage collection was declared deprecated in OS X Mountain Lion, in favour of ARC, and removed from the Objective-C runtime library in macOS Sierra.</p>
<p>To understand ARC, a developer should first understand ObjectiveC&rsquo;s &ldquo;Object ownership&rdquo; mechanism. The concept of &ldquo;Object ownership&rdquo; just implies that a class should either own and manage memory that it allocates or either delegate it to some other class.</p>
<p>Now that it is clear that ARC is not garbage collection, then what exactly is it? ARC is a kind of preprocessor that executes before the compiler and virtually inserts release and autorelease statements for us.ARC is a very helpful feature provided by XCode and its capabilities can be enhanced by helping it release memory well.ARC knows that the use of the memory is complete and hence safely releases it.</p>
<p><span style="color: #0000ff;"><strong>Objective-C</strong></span></p>
<p>The following rules are enforced by the compiler when ARC is turned on:</p>
<ul>
<li>retain, release, retainCount, autorelease or dealloc cannot be sent to objects. Instead, the compiler inserts these messages at compile time automatically, including [super dealloc] when dealloc is overridden.<sup>[9]</sup></li>
</ul>
<p><em>// Without ARC</em></p>
<p>- (void)dealloc</p>
<p>{</p>
<p>&nbsp;&nbsp; [[NSNotificationCenter defaultCenter] removeObserver:self];</p>
<p>&nbsp;&nbsp; [super dealloc];</p>
<p>}</p>
<p>&nbsp;</p>
<p><em>// With ARC</em></p>
<p>- (void)dealloc</p>
<p>{</p>
<p>&nbsp;&nbsp; [[NSNotificationCenter defaultCenter] removeObserver:self];</p>
<p>&nbsp;&nbsp; <em>// [super dealloc] is called automatically</em></p>
<p>}</p>
<ul>
<li>Programs cannot cast directly between id and void *.<sup>[9]</sup> This includes casting between Foundation objects and Core Foundation objects.</li>
</ul>
<p>Programs must use special casts, or calls to special functions, to tell the compiler more information about an object's lifetime.</p>
<p><em>// Without ARC</em></p>
<p>- (NSString *)giveMeAString</p>
<p>{</p>
<p>&nbsp; &nbsp; CFStringRef myString = [self someMethodThatCreatesACFString];</p>
<p>&nbsp; &nbsp; NSString *newString = (NSString *)myString;</p>
<p>&nbsp; &nbsp; <strong>return</strong> [newString autorelease];</p>
<p>}</p>
<p>&nbsp;</p>
<p><em>// With ARC</em></p>
<p>- (NSString *)giveMeAString</p>
<p>{</p>
<p>&nbsp; &nbsp; CFStringRef myString = [self someMethodThatCreatesACFString]; <em>// retain count is 1</em></p>
<p>&nbsp; &nbsp; NSString *newString = (<strong>__bridge_transfer</strong> NSString *)myString; <em>// the ownership has now been transferred into ARC</em></p>
<p>&nbsp; &nbsp; <strong>return</strong> newString;</p>
<p>}</p>
<ul>
<li>An autorelease pool can be used to allocate objects temporarily and retain them in memory until the pool is "drained". Without ARC, an NSAutoreleasePool object can be created for this purpose. ARC uses @autoreleasepool blocks instead, which encapsulate the allocation of the temporary objects and deallocates them when the end of the block is reached.<sup>[9]</sup></li>
</ul>
<p><em>// Without ARC</em></p>
<p>- (void)loopThroughArray:(NSArray *)array</p>
<p>{</p>
<p>&nbsp; &nbsp; <strong>for</strong> (id object <strong>in</strong> array) {</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; <em>// Create a lot of temporary objects</em></p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; [pool drain];</p>
<p>&nbsp; &nbsp; }</p>
<p>}</p>
<p><em>// With ARC</em></p>
<p>- (void)loopThroughArray:(NSArray *)array</p>
<p>{</p>
<p>&nbsp; &nbsp; <strong>for</strong> (id object <strong>in</strong> array) {</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; <strong>@autoreleasepool</strong> {</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <em>// Create a lot of temporary objects</em></p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; }</p>
<p>&nbsp; &nbsp; }</p>
<p>}</p>
<ul>
<li>Programs cannot call the functions NSAllocateObject and NSDeallocateObject</li>
<li>Programs cannot use object pointers in C structures (structs)</li>
<li>Programs cannot use memory zones (NSZone)</li>
<li>To properly cooperate with non-ARC code, programs must use no method or declared property (unless explicitly choosing a different getter) that starts with copy.</li>
</ul>