<h2><span style="color: #008080;"><strong>Categories and Extensions</strong></span></h2>
<h3><span style="color: #0000ff;">Categories</span></h3>
<p>An Objective-C category allows you to add methods to an existing class -- effectively subclassing it without having to deal with the possible complexities of subclassing.</p>
<p>Some situations in which you may want to use categories include:</p>
<ul>
<li>You want to split the functionality of a class into multiple files (for many of the same reasons you&rsquo;d want to create classes in the first place&mdash;ease of use, less to maintain at a time, etc.)</li>
<li>You&rsquo;re working with other people on the same classes and you&rsquo;ve been tasked with handling certain parts of that class (drawing to screen, for example)</li>
<li>You don&rsquo;t want to deal with subclassing a class&mdash;for example, in NSString&lsquo;s subclassing notes, it states that &ldquo;doing so requires providing storage facilities for the string (which is not inherited by subclasses) and implementing two primitive methods. The abstract NSString and NSMutableString classes are the public interface of a class cluster consisting mostly of private, concrete classes that create and return a string object appropriate for a given situation. Making your own concrete subclass of this cluster imposes certain requirements&hellip;&rdquo;</li>
<li>You need to add methods to a class that you don&rsquo;t have access to, such as the Foundation classes (NSString, NSArray)</li>
</ul>
<p>In all of these cases, categories provide a perfect solution. As an example, we are going to add a method to NSArray that will return a random, non-repeating object from an array. This could be used, perhaps, in a word game, where you might have an array of NSString words and want to use each word exactly once.</p>
<p>Jumping right in, we are going to put our category into a separate file&mdash;we don&rsquo;t have access to the original NSArray files. If we did, anything that used NSArray could use our method&hellip;but since we don&rsquo;t, we&rsquo;re going to have to import our file before we use it.</p>
<p>The naming convention of category files is OriginalClassName-CategoryName. Therefore, create two files, NSArray-RandomObjects.h and NSArray-RandomObjects.m. In the former, place this code:</p>
<ol>
<li>@interface NSArray(RandomObjects)</li>
<li>-(id)randomObject;</li>
<li>@end</li>
</ol>
<p>Note that it simply looks like a class declaration&mdash;except for the part in bold (emphasis added). That part tells the compiler that you&rsquo;re defining a category, not a new class. The name is used for documentative purposes (telling yourself what it does) and is effectively irrelevant elsewhere. And on the subject of new classes, note that you cannot add new instance variables to a class&mdash;this goes back to the dynamic nature of methods (bonus points to anyone who can explain why).</p>
<p>In the corresponding .m file, place the implementation of that method, as usual:</p>
<ol>
<li>@implementation NSArray(RandomObjects) - (id)randomObject { // You could also pass in an array as an argument</li>
<li>&nbsp; &nbsp; self.randomArray = [[self.randomArray shuffledArray] mutableCopy];</li>
<li>&nbsp; &nbsp; NSInteger index = arc4random() % [self.randomArray count] - 1;</li>
<li>&nbsp; &nbsp; id object = [self.randomArray objectAtIndex:index];</li>
<li>&nbsp; &nbsp; [self.randomArray removeObjectAtIndex:index];</li>
<li>&nbsp; &nbsp; return object;</li>
<li>}</li>
<li>@end</li>
</ol>
<p>Note again the bolded text&mdash;that is the only syntactical difference between a category and a class definition.</p>
<p>A test program might look like this:</p>
<ol>
<li>#import "NSArray-RandomObjects.h"</li>
<li>int main(int argc, char *argv[]) {</li>
<li>&nbsp; &nbsp; NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];</li>
<li>&nbsp; &nbsp; NSArray *words = [NSArray arrayWithObjects:@"Curb", @"Days", @"Fins", @"Dish", @"Cite", @"Chat", nil];</li>
<li>&nbsp; &nbsp; for (NSInteger count = 1; count &lt; [words count]; count++)</li>
<li>&nbsp; &nbsp; &nbsp; &nbsp; NSLog(@"Next object is %@", [words randomObject]);</li>
<li>}</li>
</ol>
<p>If you try to build and run now, you&rsquo;ll get a compiler warning. And thin the program will crash. Turns out, the randomArray method we&rsquo;re using in our category method comes from yet another category. Check it out, import the new header in NSArray-RandomObjects.h, and you should be good to go.</p>
<p><span style="color: #0000ff;"><strong>A Few Notes</strong></span></p>
<ul>
<li>A category method can override an existing method, but you&rsquo;ll lose access to the original outside of that class. Therefore, if you do override, make sure you duplicate existing functionality, or use a call to super.</li>
<li>If a method is defined in more than one category of a single class, which version is invoked is undefined.</li>
<li>Remember that methods that you add to a class are inherited as well. If you add methods to NSObject, every class will have access to those methods.</li>
<li>Added methods may serve your purpose, but may go against the design of the class. Adding the ability to handle multiple strings simultaneously to NSString blurs the line between NSString and NSArray.</li>
<li>Object / category name pairs must be unique in any namespace. There can only be one NSArray-RandomObjects in a namespace. The default namespace is shared between all classes, libraries, frameworks, and plug-ins. This can be a hazard for developers of those products, especially since they get injected into other code.</li>
</ul>
<p>Sometimes, you may find that you wish to extend an existing class by adding behavior that is useful only in certain situations. In order add such extension to existing classes, Objective-C provides <strong>categories</strong> and <strong>extensions</strong>.</p>
<p>If you need to add a method to an existing class, perhaps, to add functionality to make it easier to do something in your own application, the easiest way is to use a category.</p>
<p>The syntax to declare a category uses the @interface keyword, just like a standard Objective-C class description, but does not indicate any inheritance from a subclass. Instead, it specifies the name of the category in parentheses, like this:</p>
<p>@interface ClassName (CategoryName)</p>
<p>@end</p>
<p>Characteristics of category</p>
<ul>
<li>A category can be declared for any class, even if you don't have the original implementation source code.</li>
<li>Any methods that you declare in a category will be available to all instances of the original class, as well as any subclasses of the original class.</li>
<li>At runtime, there's no difference between a method added by a category and one that is implemented by the original class.</li>
</ul>
<p>Now, let's look at a sample category implementation. Let's add a category to the Cocoa class NSString. This category will make it possible for us to add a new method getCopyRightString which helps us in returning the copyright string. It is shown below.</p>
<p>#import &lt;Foundation/Foundation.h&gt;</p>
<p>@interface NSString(MyAdditions)</p>
<p>+(NSString *)getCopyRightString;</p>
<p>@end</p>
<p>@implementation NSString(MyAdditions)</p>
<p>+(NSString *)getCopyRightString{</p>
<p>&nbsp; &nbsp; return @"Copyright TutorialsPoint.com 2013";</p>
<p>}</p>
<p>@end</p>
<p>int main(int argc, const char * argv[])</p>
<p>{</p>
<p>&nbsp; &nbsp;NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];</p>
<p>&nbsp;&nbsp; NSString *copyrightString = [NSString getCopyRightString];</p>
<p>&nbsp;&nbsp; NSLog(@"Accessing Category: %@",copyrightString);</p>
<p>&nbsp;&nbsp; [pool drain];</p>
<p>&nbsp; &nbsp; return 0;</p>
<p>}</p>
<h3><span style="color: #0000ff;">Extensions</span></h3>
<p>A class extension bears some similarity to a category, but it can only be added to a class for which you have the source code at compile time (the class is compiled at the same time as the class extension).</p>
<p>The methods declared by a class extension are implemented in the implementation block for the original class, so you can't, for example, declare a class extension on a framework class, such as a Cocoa or Cocoa Touch class like NSString..</p>
<p>Extensions are actually categories without the category name. It's often referred as <strong>anonymous categories</strong>.</p>
<p>The syntax to declare a extension uses the @interface keyword, just like a standard Objective-C class description, but does not indicate any inheritance from a subclass. Instead, it just adds parentheses, as shown below</p>
<p>@interface ClassName ()</p>
<p>@end</p>
<p><span style="color: #0000ff;">Characteristics of extensions</span></p>
<ul>
<li>An extension cannot be declared for any class, only for the classes that we have original implementation of source code.</li>
<li>An extension is adding private methods and private variables that are only specific to the class.</li>
<li>Any method or variable declared inside the extensions is not accessible even to the inherited classes.</li>
</ul>
<p>Extensions Example</p>
<p>Let's create a class SampleClass that has an extension. In the extension, let's have a private variable internalID.</p>
<p>Then, let's have a method getExternalID that returns the externalID after processing the internalID.</p>
<p>The example is shown below and this wont work on online compiler.</p>
<p>#import &lt;Foundation/Foundation.h&gt;</p>
<p>@interface SampleClass : NSObject</p>
<p>{</p>
<p>&nbsp; &nbsp; NSString *name;</p>
<p>}</p>
<p>- (void)setInternalID;</p>
<p>- (NSString *)getExternalID;</p>
<p>@end</p>
<p>@interface SampleClass()</p>
<p>{</p>
<p>&nbsp; &nbsp; NSString *internalID;</p>
<p>}</p>
<p>@end</p>
<p>@implementation SampleClass</p>
<p>- (void)setInternalID{</p>
<p>&nbsp; &nbsp; internalID = [NSString stringWithFormat:&nbsp;</p>
<p>&nbsp; &nbsp; @"UNIQUEINTERNALKEY%dUNIQUEINTERNALKEY",arc4random()%100];</p>
<p>}</p>
<p>- (NSString *)getExternalID{</p>
<p>&nbsp; &nbsp; return [internalID stringByReplacingOccurrencesOfString:&nbsp;</p>
<p>&nbsp; &nbsp; @"UNIQUEINTERNALKEY" withString:@""];</p>
<p>}</p>
<p>@end</p>
<p>int main(int argc, const char * argv[])</p>
<p>{</p>
<p>&nbsp; &nbsp; NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];</p>
<p>&nbsp; &nbsp; SampleClass *sampleClass = [[SampleClass alloc]init];</p>
<p>&nbsp; &nbsp; [sampleClass setInternalID];</p>
<p>&nbsp; &nbsp; NSLog(@"ExternalID: %@",[sampleClass getExternalID]); &nbsp; &nbsp; &nbsp; &nbsp;</p>
<p>&nbsp; &nbsp; [pool drain];</p>
<p>&nbsp; &nbsp; return 0;</p>
<p>}</p>
<p>&nbsp;</p>