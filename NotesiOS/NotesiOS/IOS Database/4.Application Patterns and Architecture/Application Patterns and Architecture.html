<div class="page" title="Page 2">
<div class="layoutArea">
<div class="column">
<h2><span style="color: #008080;"><strong>Model View Controller (MVC)</strong></span></h2>
<p>As implied with the name, the MVC software design pattern refers to three separate roles: the model, the view and the controller.</p>
<p>The idea is that the objects in your application will take on these roles and work together to create and manage the user interface. Not all objects will assume one of these 3 roles; For example you might have a class that contains a bunch of helper methods however, the model, view and controller roles are the central actors to making your app function.</p>
<p>The goal of the MVC design pattern is to divorce the logic and data handling code of an application from the presentation code. In this concept, the Model encapsulates the data for the application, the View presents and manages the user interface and the Controller provides the basic logic for the application and acts as the go-between, providing instructions to the Model based on user interactions with the View and updating the View to reflect responses from the Model. The true value of this approach is that the Model knows absolutely nothing about the presentation of the application. It just knows how to store and handle data and perform certain tasks when called upon by the Controller. Similarly, the View knows nothing about the data and logic model of the application.</p>
<p><strong>The Model</strong></p>
<p>The model represents the data in your application. It&rsquo;s responsible for sorting, validating and organizing your data.</p>
<p><strong>The View</strong></p>
<p>The view is the user interface or what the user sees and interacts with.</p>
<p>You can create views programmatically through code using Apple classes such as UIView or you can create a XIB file to represent the view.</p>
<p><strong>The Controller</strong></p>
<p>The controller manages the communication between the view and the model. It takes the data from the model and communicates it to the view for display.</p>
<p>In the same vein, the controller also takes the changed data and communicates it back to the model.</p>
<p><strong>The Model And View Never Talk To Each Other</strong></p>
<p>They shouldn&rsquo;t even know about each other in the ideal case.</p>
<p>The beauty of this modular architecture is that the separation of roles allow us to make modifications easily with less bugs.</p>
<p>Here&rsquo;s a diagram illustrating what we&rsquo;ve talked about:</p>
<p>image</p>
<h3><span id="Delegation" class="mw-headline">Delegation</span></h3>
<p>Delegation allows an object to pass the responsibility for performing one or more tasks on to another object. This allows the behavior of an object to be modified without having to go through the process of subclassing it.</p>
<p>A prime example of delegation can be seen in the case of the UIApplication class. The UIApplication class, of which every iOS iPhone application must have one (and only one) instance, is responsible for the control and operation of the application within the iOS environment. Much of what the UIApplication object does happens in the background. There are, however, instances where it gives us the opportunity to include our own functionality into the mix. UIApplication allows us to do this by delegating some methods to us. As an example, UIApplication delegates the didFinishLaunchingWithOptions: method to us so that we can write code to perform specific tasks when the app first loads (for example taking the user back to the point they were at when they last exited). If you still have a copy of the Hello World project created earlier in this book you will see the template for this method in the HelloWorldAppDelegate.m file.</p>
<h2><span style="color: #008080;"><strong>IBOutlets, IBActions and&nbsp;IBOutletCollection</strong></span></h2>
<p>IBOutlet and IBAction were used as keywords, to denote what parts of the code should be visible to Interface Builder.</p>
<p>#define IBAction void</p>
<p>#define IBOutlet</p>
<p>#define IBOutlet __attribute__((iboutlet))</p>
<p>#define IBAction __attribute__((ibaction))</p>
<h3><strong>IBAction</strong></h3>
<p>Any method with the signature -(void){name}:(id)sender would be visible in the outlets pane.</p>
<p>Nevertheless, many developers find it useful to still use the IBAction return type in method declarations to denote that a particular method is connected to by an action.</p>
<p><strong>Naming IBAction Methods</strong></p>
<ul>
<li><strong>Return type of </strong><strong>IBAction</strong><strong>.</strong></li>
<li><strong>Method name of an active verb, describing the specific action performed.</strong> Method names like didTapButton: or didPerformAction: sound more like things a delegate might be sent.</li>
<li><strong>Required </strong><strong>sender</strong><strong> parameter of type </strong><strong>id</strong><strong>.</strong> All target / action methods will pass the sender of the action (usually the responder) to methods that take a parameter. If omitted in the method signature, things will still work.</li>
</ul>
<p>For example:</p>
<p>// YES</p>
<p>- (<strong>IBAction</strong>)refresh:(<strong>id</strong>)sender;</p>
<p>- (<strong>IBAction</strong>)toggleVisibility:(<strong>id</strong>)sender</p>
<p>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; withEvent:(UIEvent *)event;</p>
<p>// NO</p>
<p>- (<strong>IBAction</strong>)peformSomeAction;</p>
<p>- (<strong>IBAction</strong>)didTapButton:(<strong>id</strong>)sender;</p>
<h3><strong>IBOutlet</strong></h3>
<p>Unlike IBAction, IBOutlet is still required for hooking up properties in code with objects in a Storyboard or XIB.</p>
<p>An IBOutlet connection is usually established between a view or control and its managing view controller (this is often done in addition to any IBActions that a view controller might be targeted to perform by a responder). However, an IBOutlet can also be used to expose a top-level property, like another controller or a property that could then be accessed by a referencing view controller.</p>
<p>// YES</p>
<p>@interface GallantViewController : UIViewController</p>
<p>@property (nonatomic, weak) <strong>IBOutlet</strong> UISwitch *switch;</p>
<p>@end</p>
<p>&nbsp;</p>
<p>// NO</p>
<p>@interface GoofusViewController : UIViewController {</p>
<p>&nbsp; &nbsp; <strong>IBOutlet</strong> UISwitch *_switch</p>
<p>}</p>
<p>@end</p>
<p><strong>When to use </strong><strong>weak</strong><strong> or </strong><strong>strong</strong></p>
<p><strong>Always declare </strong><strong>IBOutlet</strong><strong> properties as </strong><strong>weak</strong><strong>, except when they need to be </strong><strong>strong</strong></p>
<p>Outlets should be changed to strong when the outlet should be considered to own the referenced object:</p>
<p>The reason why most IBOutlet views can get away with weak ownership is that they are already owned within their respective view hierarchy, by their superview. This chain of ownership eventually works its way up to the view owned by the view controller itself. Spurious use of strong ownership on a view outlet has the potential to create a retain cycle.</p>
<h3><strong>IBOutletCollection</strong></h3>
<p>IBOutlets to be defined in Interface Builder, by dragging connections to its collection members.</p>
<p>IBOutletCollection is #define&rsquo;d in UINibDeclarations.h as:</p>
<p>#define IBOutletCollection(ClassName)</p>
<p>&hellip;which is defined in a much more satisfying way, again, <a href="http://opensource.apple.com/source/clang/clang-318.0.45/src/tools/clang/test/SemaObjC/iboutletcollection-attr.m">in the Clang source code</a>:</p>
<p>#define IBOutletCollection(ClassName) __attribute__((iboutletcollection(ClassName)))</p>
<p>Unlike IBAction or IBOutlet, IBOutletCollection takes a class name as an argument, which is, incidentally, as close to Apple-sanctioned <a href="http://en.wikipedia.org/wiki/Generic_programming">generics</a> as one gets in Objective-C.</p>
<p>As a top-level object, an IBOutletCollection @property should be declared strong, with an NSArray * type:</p>
<p>@property (nonatomic, strong) IBOutletCollection(UIButton) NSArray *buttons;</p>
<p>With the advent of Objective-C <a href="http://nshipster.com/at-compiler-directives/">object literals</a>, IBOutletCollection has fallen slightly out of favor&mdash;at least for the common use case of convenience accessors, as in:</p>
<p>for (UILabel *label in labels) {</p>
<p>&nbsp; &nbsp; label.font = [UIFont systemFontOfSize:<strong>14</strong>];</p>
<p>}</p>
<p>Since declaring a collection of outlets is now as easy as comma-delimiting them within @[], it may make just as much sense to do that as create a distinct collection.</p>
<p>Where IBOutletCollection really shines is how it allows you to define a unique collection of outlets under a shared identifier.&nbsp;</p>
<p>IBAction, IBOutlet, and IBOutletCollection play important roles in development, on both the compiler level and human level.</p>
<h2><span style="color: #008080;"><strong>Subclassing and Delegation</strong></span></h2>
<p>The delegation pattern is widely used throughout the Cocoa frameworks, for good reason. A delegate customizes another object&rsquo;s behavior in a lightweight way, with less coupling than a subclass. A single object can be the delegate of several others, so a set of controls presented to the user as a logical group can be managed by code that&rsquo;s also kept together.</p>
<p><strong>Interface or implementation?</strong></p>
<p>List delegate protocols in the public interface of a class.</p>
<p>Let&rsquo;s consider a chain of three classes: SpecificViewController : BaseViewController : UIViewController. BaseViewController contains behavior common to several views of our app, and has several subclasses, including SpecificViewController. This behavior includes managing a UITextField, and so it conforms to the &lt;UITextFieldDelegate&gt; protocol.</p>
<p><strong>BaseViewController.h</strong></p>
<p><strong>@interface</strong> <strong>BaseViewController</strong> : UIViewController</p>
<p><strong>@end</strong></p>
<p><strong>BaseViewController.m</strong></p>
<p><strong>#import "BaseViewController.h"</strong></p>
<p><strong>@interface</strong> <strong>BaseViewController</strong> () <strong>&lt;</strong>UITextFieldDelegate<strong>&gt;</strong></p>
<p><strong>@property</strong> (<strong>nonatomic</strong>) <strong>IBOutlet</strong> UITextField <strong>*</strong>baseTextField;</p>
<p><strong>@end</strong></p>
<p><strong>@implementation</strong> <strong>BaseViewController</strong></p>
<p>- (<strong>void</strong>)<strong>textFieldDidEndEditing:</strong>(UITextField <strong>*</strong>)textField {</p>
<p>&nbsp; &nbsp; <strong>if</strong> (textField <strong>==</strong> self.baseTextField) {</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; NSLog(@"%s with %@", __func__, textField.text);</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; <em>// ...</em></p>
<p>&nbsp; &nbsp; }</p>
<p>}</p>
<p><strong>@end</strong></p>
<p>At first glance this seems like a good approach, and makes our encapsulation senses tingle with joy. But it has problems.</p>
<p>Someone else on the team is working on a subclass, SpecificViewController, which needs an extra text field. So it also conforms to &lt;UITextFieldDelegate&gt;:</p>
<p><strong>SpecificViewController.m</strong></p>
<p><strong>#import "SpecificViewController.h"</strong></p>
<p><strong>@interface</strong> <strong>SpecificViewController</strong> () <strong>&lt;</strong>UITextFieldDelegate<strong>&gt;</strong></p>
<p><strong>@property</strong> (<strong>nonatomic</strong>) <strong>IBOutlet</strong> UITextField <strong>*</strong>secondTextField;</p>
<p><strong>@end</strong></p>
<p><strong>@implementation</strong> <strong>SpecificViewController</strong></p>
<p>- (<strong>void</strong>)<strong>textFieldDidEndEditing:</strong>(UITextField <strong>*</strong>)textField {</p>
<p>&nbsp; &nbsp; <strong>if</strong> (textField <strong>==</strong> self.secondTextField) {</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; <em>// ...</em></p>
<p>&nbsp; &nbsp; }</p>
<p>}</p>
<p><strong>@end</strong></p>
<p>Have you noticed the problem? The behavior attached to baseTextField has silently been lost! The subclass hasn&rsquo;t called super, but calling super would generate a compiler warning, because the protocol was listed in the superclass&rsquo;s private implementation and not public interface.</p>
<p>The solution to this is to <strong>declare delegate protocol conformance in a class&rsquo;s public interface</strong>, and <strong>call super from overridden delegate methods</strong>. Declaring protocol conformance in the implementation is therefore often inappropriate.</p>
<p>However, there&rsquo;s another complication.</p>
<p><strong>Forwarding optional methods</strong></p>
<p>Cocoa delegate protocols2 have the slightly unusual feature of optional methods. This speeds up prototyping and development &mdash; just implement what you want to use &mdash; but interacts with subclassing in a subtle way.</p>
<p>Many delegate methods are @optional. Before calling one, you&rsquo;re responsible for checking whether it&rsquo;s available; the compiler doesn&rsquo;t do any checking. We&rsquo;ve moved the delegate protocol into the BaseViewController.h header file. Now subclasses can know that the superclass cares about the protocol, so they should forward the methods. Let&rsquo;s adjust SpecificViewController.m:</p>
<p><strong>SpecificViewController.m</strong></p>
<p><strong>@implementation</strong> <strong>SpecificViewController</strong></p>
<p>- (<strong>void</strong>)<strong>textFieldDidEndEditing:</strong>(UITextField <strong>*</strong>)textField {</p>
<p>&nbsp; &nbsp; <strong>if</strong> (textField <strong>==</strong> self.secondTextField) {</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; <em>// ...</em></p>
<p>&nbsp; &nbsp; } <strong>else</strong> {</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; [super textFieldDidEndEditing:textField];</p>
<p>&nbsp; &nbsp; }</p>
<p>}</p>
<p><strong>@end</strong></p>
<p>Looks good, works great. But secondTextField needs some more behavior, so it adds another delegate method:</p>
<p><strong>@implementation</strong> <strong>SpecificViewController</strong></p>
<p>- (<strong>void</strong>)<strong>textFieldDidEndEditing:</strong>(UITextField <strong>*</strong>)textField {</p>
<p>&nbsp; &nbsp; <em>// ... as above</em></p>
<p>}</p>
<p>- (<strong>BOOL</strong>)<strong>textFieldShouldClear:</strong>(UITextField <strong>*</strong>)textField {</p>
<p>&nbsp; &nbsp; <strong>if</strong> (textField <strong>==</strong> self.secondTextField) {</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; <strong>return</strong> [self someConditionIsMet];</p>
<p>&nbsp; &nbsp; } <strong>else</strong> {</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; <strong>return</strong> [super textFieldShouldClear:textField];</p>
<p>&nbsp; &nbsp; }</p>
<p>}</p>
<p><strong>@end</strong></p>
<p>We try it out, it works as expected. But then someone presses the &ldquo;clear&rdquo; button in baseTextField, and&hellip; <em>crash!</em></p>
<p>-textFieldShouldClear: is an optional method in &lt;UITextFieldDelegate&gt;, so the onus is on the caller to check if it&rsquo;s safe to call &mdash; in this case, it&rsquo;s not. So how can we check?</p>
<p><strong>Look through the source code of all superclasses.</strong></p>
<p>We see that BaseViewController doesn&rsquo;t implement this method, so we can remove the call to super. But then if it ever adds it, the behavior will be silently lost again! With this approach, adding or removing a delegate method requires auditing all superclasses and all subclasses &mdash; <strong>not a scalable solution</strong>.</p>
<p>Worse: If you miss something, the code will still compile and run. Everything will likely look fine on the surface, but have a bug lurking below.</p>
<p><strong>-respondsToSelector:</strong><strong>, of course</strong></p>
<p>When we implement an object that takes its own delegate, the rule is easy: Guard all calls to @optional methods by -respondsToSelector:, like this:</p>
<p>- (<strong>BOOL</strong>)<strong>canBecomeFirstResponder</strong> {</p>
<p>&nbsp; &nbsp; <strong>if</strong> ([self.delegate respondsToSelector:<strong>@selector</strong>(myControlCanBecomeFirstResponder:)]) {</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; <strong>return</strong> [self.delegate myControlCanBecomeFirstResponder:self];</p>
<p>&nbsp; &nbsp; } <strong>else</strong> {</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; <strong>return</strong> YES;</p>
<p>&nbsp; &nbsp; }</p>
<p>}</p>
<p>Inside -[SpecificViewController textFieldShouldClear:] the selector we want to check is the same as the method we&rsquo;re in, so we can just refer to it as _cmd.3 Now obviously [self respondsToSelector:_cmd] will return true, because we&rsquo;re in that very method right now. So does [super respondsToSelector:_cmd] perform the check we want?</p>
<p><strong>No.</strong> The quick-and-dirty translation into English reads, &ldquo;ask super if it responds to the selector in _cmd,&rdquo; but that&rsquo;s wrong and misleading.</p>
<p>We need to think more precisely about what the super call <em>actually</em> means. Which is, &ldquo;call the superclass&rsquo;s implementation of -respondsToSelector:, passing it _cmd&rdquo;. Laid out like this, the behavior is clear: SpecificViewController hasn&rsquo;t overridden -respondsToSelector:, which means [self respondsToSelector:] and [super respondsToSelector:] are exactly equivalent, both most likely using NSObject&rsquo;s implementation.</p>
<p><strong>+instancesRespondToSelector:</strong><strong>, nice to meet you</strong></p>
<p>This is an oft forgotten NSObject method which does exactly as it sounds: performs -respondsToSelector:, but at the class level.</p>
<p>So we can do this:</p>
<p><strong>@implementation</strong> <strong>SpecificViewController</strong></p>
<p>- (<strong>BOOL</strong>)<strong>textFieldShouldClear:</strong>(UITextField <strong>*</strong>)textField {</p>
<p>&nbsp; &nbsp; <strong>if</strong> (textField <strong>==</strong> self.secondTextField) {</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; <strong>return</strong> [self someConditionIsMet];</p>
<p>&nbsp; &nbsp; } <strong>else</strong> <strong>if</strong> ([BaseViewController instancesRespondToSelector:_cmd]) {</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; <strong>return</strong> [super textFieldShouldClear:textField];</p>
<p>&nbsp; &nbsp; } <strong>else</strong> {</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; <strong>return</strong> YES;</p>
<p>&nbsp; &nbsp; }</p>
<p>}</p>
<p><strong>@end</strong></p>
<p><strong>This is correct!</strong> Note that we also had to hard-code the default value in case the superclass doesn&rsquo;t respond.4</p>
<p>It&rsquo;s ugly to hard-code the superclass in there like that, so how about [[[self class] superclass] instancesRespondToSelector:_cmd]? This <em>feels like</em> it should be the same, but it&rsquo;s booby-trapped! When someone else comes along and declares another subclass, EvenMoreSpecificViewController : SpecificViewController, then [self class] is EvenMoreSpecificViewController and [[self class] superclass] is SpecificViewController. That check would succeed, there&rsquo;d be a call to super (BaseViewController), then a crash. So nope.</p>
<p>It&rsquo;s slightly cleaner is to use the defining class: [[SpecificViewController superclass] instancesRespondToSelector:_cmd]. This adds safety in case a class ever gets split into two, changing the superclass. But it&rsquo;s still an absolute reference, so the code isn&rsquo;t a nice cut-and-paste snippet.</p>
<p>It&rsquo;d be nice to have a way to reference the defining class &mdash; like the __FILE__ macro but for the current @implementation. Although there&rsquo;s nothing built-in, it&rsquo;s possible to add with a macro (though less efficiently than one provided by the compiler). I&rsquo;ve done that in the <a href="https://github.com/jmah/MyLilKeyPathHelpers">MyLilKeyPathHelpers</a> project, called _definingClass. Using this, we can create a copy-and-paste snippet: [[_definingClass superclass] instancesRespondToSelector:_cmd].</p>
<p>&nbsp;</p>
</div>
</div>
</div>