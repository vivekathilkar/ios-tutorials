<h2><span style="color: #008080;"><strong>Subclassing and Delegation</strong></span></h2>
<p>The delegation pattern is widely used throughout the Cocoa frameworks, for good reason. A delegate customizes another object&rsquo;s behavior in a lightweight way, with less coupling than a subclass. A single object can be the delegate of several others, so a set of controls presented to the user as a logical group can be managed by code that&rsquo;s also kept together.</p>
<p><strong>Interface or implementation?</strong></p>
<p>List delegate protocols in the public interface of a class.</p>
<p>Let&rsquo;s consider a chain of three classes: SpecificViewController : BaseViewController : UIViewController. BaseViewController contains behavior common to several views of our app, and has several subclasses, including SpecificViewController. This behavior includes managing a UITextField, and so it conforms to the &lt;UITextFieldDelegate&gt; protocol.</p>
<p><strong>BaseViewController.h</strong></p>
<p><strong>@interface</strong> <strong>BaseViewController</strong> : UIViewController</p>
<p><strong>@end</strong></p>
<p><strong>BaseViewController.m</strong></p>
<p><strong>#import "BaseViewController.h"</strong></p>
<p><strong>@interface</strong> <strong>BaseViewController</strong> () <strong>&lt;</strong>UITextFieldDelegate<strong>&gt;</strong></p>
<p><strong>@property</strong> (<strong>nonatomic</strong>) <strong>IBOutlet</strong> UITextField <strong>*</strong>baseTextField;</p>
<p><strong>@end</strong></p>
<p><strong>@implementation</strong> <strong>BaseViewController</strong></p>
<p>- (<strong>void</strong>)<strong>textFieldDidEndEditing:</strong>(UITextField <strong>*</strong>)textField {</p>
<p>&nbsp; &nbsp; <strong>if</strong> (textField <strong>==</strong> self.baseTextField) {</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; NSLog(@"%s with %@", __func__, textField.text);</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; <em>// ...</em></p>
<p>&nbsp; &nbsp; }</p>
<p>}</p>
<p><strong>@end</strong></p>
<p>At first glance this seems like a good approach, and makes our encapsulation senses tingle with joy. But it has problems.</p>
<p>Someone else on the team is working on a subclass, SpecificViewController, which needs an extra text field. So it also conforms to &lt;UITextFieldDelegate&gt;:</p>
<p><strong>SpecificViewController.m</strong></p>
<p><strong>#import "SpecificViewController.h"</strong></p>
<p><strong>@interface</strong> <strong>SpecificViewController</strong> () <strong>&lt;</strong>UITextFieldDelegate<strong>&gt;</strong></p>
<p><strong>@property</strong> (<strong>nonatomic</strong>) <strong>IBOutlet</strong> UITextField <strong>*</strong>secondTextField;</p>
<p><strong>@end</strong></p>
<p><strong>@implementation</strong> <strong>SpecificViewController</strong></p>
<p>- (<strong>void</strong>)<strong>textFieldDidEndEditing:</strong>(UITextField <strong>*</strong>)textField {</p>
<p>&nbsp; &nbsp; <strong>if</strong> (textField <strong>==</strong> self.secondTextField) {</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; <em>// ...</em></p>
<p>&nbsp; &nbsp; }</p>
<p>}</p>
<p><strong>@end</strong></p>
<p>Have you noticed the problem? The behavior attached to baseTextField has silently been lost! The subclass hasn&rsquo;t called super, but calling super would generate a compiler warning, because the protocol was listed in the superclass&rsquo;s private implementation and not public interface.</p>
<p>The solution to this is to <strong>declare delegate protocol conformance in a class&rsquo;s public interface</strong>, and <strong>call super from overridden delegate methods</strong>. Declaring protocol conformance in the implementation is therefore often inappropriate.</p>
<p>However, there&rsquo;s another complication.</p>
<p><strong>Forwarding optional methods</strong></p>
<p>Cocoa delegate protocols2 have the slightly unusual feature of optional methods. This speeds up prototyping and development &mdash; just implement what you want to use &mdash; but interacts with subclassing in a subtle way.</p>
<p>Many delegate methods are @optional. Before calling one, you&rsquo;re responsible for checking whether it&rsquo;s available; the compiler doesn&rsquo;t do any checking. We&rsquo;ve moved the delegate protocol into the BaseViewController.h header file. Now subclasses can know that the superclass cares about the protocol, so they should forward the methods. Let&rsquo;s adjust SpecificViewController.m:</p>
<p><strong>SpecificViewController.m</strong></p>
<p><strong>@implementation</strong> <strong>SpecificViewController</strong></p>
<p>- (<strong>void</strong>)<strong>textFieldDidEndEditing:</strong>(UITextField <strong>*</strong>)textField {</p>
<p>&nbsp; &nbsp; <strong>if</strong> (textField <strong>==</strong> self.secondTextField) {</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; <em>// ...</em></p>
<p>&nbsp; &nbsp; } <strong>else</strong> {</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; [super textFieldDidEndEditing:textField];</p>
<p>&nbsp; &nbsp; }</p>
<p>}</p>
<p><strong>@end</strong></p>
<p>Looks good, works great. But secondTextField needs some more behavior, so it adds another delegate method:</p>
<p><strong>@implementation</strong> <strong>SpecificViewController</strong></p>
<p>- (<strong>void</strong>)<strong>textFieldDidEndEditing:</strong>(UITextField <strong>*</strong>)textField {</p>
<p>&nbsp; &nbsp; <em>// ... as above</em></p>
<p>}</p>
<p>- (<strong>BOOL</strong>)<strong>textFieldShouldClear:</strong>(UITextField <strong>*</strong>)textField {</p>
<p>&nbsp; &nbsp; <strong>if</strong> (textField <strong>==</strong> self.secondTextField) {</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; <strong>return</strong> [self someConditionIsMet];</p>
<p>&nbsp; &nbsp; } <strong>else</strong> {</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; <strong>return</strong> [super textFieldShouldClear:textField];</p>
<p>&nbsp; &nbsp; }</p>
<p>}</p>
<p><strong>@end</strong></p>
<p>We try it out, it works as expected. But then someone presses the &ldquo;clear&rdquo; button in baseTextField, and&hellip; <em>crash!</em></p>
<p>-textFieldShouldClear: is an optional method in &lt;UITextFieldDelegate&gt;, so the onus is on the caller to check if it&rsquo;s safe to call &mdash; in this case, it&rsquo;s not. So how can we check?</p>
<p><strong>Look through the source code of all superclasses.</strong></p>
<p>We see that BaseViewController doesn&rsquo;t implement this method, so we can remove the call to super. But then if it ever adds it, the behavior will be silently lost again! With this approach, adding or removing a delegate method requires auditing all superclasses and all subclasses &mdash; <strong>not a scalable solution</strong>.</p>
<p>Worse: If you miss something, the code will still compile and run. Everything will likely look fine on the surface, but have a bug lurking below.</p>
<p><strong>-respondsToSelector:</strong><strong>, of course</strong></p>
<p>When we implement an object that takes its own delegate, the rule is easy: Guard all calls to @optional methods by -respondsToSelector:, like this:</p>
<p>- (<strong>BOOL</strong>)<strong>canBecomeFirstResponder</strong> {</p>
<p>&nbsp; &nbsp; <strong>if</strong> ([self.delegate respondsToSelector:<strong>@selector</strong>(myControlCanBecomeFirstResponder:)]) {</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; <strong>return</strong> [self.delegate myControlCanBecomeFirstResponder:self];</p>
<p>&nbsp; &nbsp; } <strong>else</strong> {</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; <strong>return</strong> YES;</p>
<p>&nbsp; &nbsp; }</p>
<p>}</p>
<p>Inside -[SpecificViewController textFieldShouldClear:] the selector we want to check is the same as the method we&rsquo;re in, so we can just refer to it as _cmd.3 Now obviously [self respondsToSelector:_cmd] will return true, because we&rsquo;re in that very method right now. So does [super respondsToSelector:_cmd] perform the check we want?</p>
<p><strong>No.</strong> The quick-and-dirty translation into English reads, &ldquo;ask super if it responds to the selector in _cmd,&rdquo; but that&rsquo;s wrong and misleading.</p>
<p>We need to think more precisely about what the super call <em>actually</em> means. Which is, &ldquo;call the superclass&rsquo;s implementation of -respondsToSelector:, passing it _cmd&rdquo;. Laid out like this, the behavior is clear: SpecificViewController hasn&rsquo;t overridden -respondsToSelector:, which means [self respondsToSelector:] and [super respondsToSelector:] are exactly equivalent, both most likely using NSObject&rsquo;s implementation.</p>
<p><strong>+instancesRespondToSelector:</strong><strong>, nice to meet you</strong></p>
<p>This is an oft forgotten NSObject method which does exactly as it sounds: performs -respondsToSelector:, but at the class level.</p>
<p>So we can do this:</p>
<p><strong>@implementation</strong> <strong>SpecificViewController</strong></p>
<p>- (<strong>BOOL</strong>)<strong>textFieldShouldClear:</strong>(UITextField <strong>*</strong>)textField {</p>
<p>&nbsp; &nbsp; <strong>if</strong> (textField <strong>==</strong> self.secondTextField) {</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; <strong>return</strong> [self someConditionIsMet];</p>
<p>&nbsp; &nbsp; } <strong>else</strong> <strong>if</strong> ([BaseViewController instancesRespondToSelector:_cmd]) {</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; <strong>return</strong> [super textFieldShouldClear:textField];</p>
<p>&nbsp; &nbsp; } <strong>else</strong> {</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; <strong>return</strong> YES;</p>
<p>&nbsp; &nbsp; }</p>
<p>}</p>
<p><strong>@end</strong></p>
<p><strong>This is correct!</strong> Note that we also had to hard-code the default value in case the superclass doesn&rsquo;t respond.4</p>
<p>It&rsquo;s ugly to hard-code the superclass in there like that, so how about [[[self class] superclass] instancesRespondToSelector:_cmd]? This <em>feels like</em> it should be the same, but it&rsquo;s booby-trapped! When someone else comes along and declares another subclass, EvenMoreSpecificViewController : SpecificViewController, then [self class] is EvenMoreSpecificViewController and [[self class] superclass] is SpecificViewController. That check would succeed, there&rsquo;d be a call to super (BaseViewController), then a crash. So nope.</p>
<p>It&rsquo;s slightly cleaner is to use the defining class: [[SpecificViewController superclass] instancesRespondToSelector:_cmd]. This adds safety in case a class ever gets split into two, changing the superclass. But it&rsquo;s still an absolute reference, so the code isn&rsquo;t a nice cut-and-paste snippet.</p>
<p>It&rsquo;d be nice to have a way to reference the defining class &mdash; like the __FILE__ macro but for the current @implementation. Although there&rsquo;s nothing built-in, it&rsquo;s possible to add with a macro (though less efficiently than one provided by the compiler). I&rsquo;ve done that in the <a href="https://github.com/jmah/MyLilKeyPathHelpers">MyLilKeyPathHelpers</a> project, called _definingClass. Using this, we can create a copy-and-paste snippet: [[_definingClass superclass] instancesRespondToSelector:_cmd].</p>
<p>&nbsp;</p>
</div>
</div>
</div>