<div class="page" title="Page 2">
<div class="layoutArea">
<div class="column">
<div class="page" title="Page 2">
<div class="layoutArea">
<div class="column">
<h2><span style="color: #008080;">Introduction</span></h2>
<p>Storyboarding is a feature built into Xcode that allows both the various screens that comprise an iOS application and the navigation path through those screens to be visually assembled. Using the Interface Builder component of Xcode, the developer simply drags and drops view and navigation controllers onto a canvas and designs the user interface of each view in the normal manner. The developer then drags lines to link individual trigger controls (such as a button) to the corresponding view controllers that are to be displayed when the control is selected by the user. Having designed both the screens (referred to in the context of storyboarding as scenes) and specified the transitions between scenes (referred to as segues) Xcode generates all the code necessary to implement the defined behavior in the completed application. The style of transition for each segue (page fold, cross dissolve etc) may also be defined within Interface Builder. Further, segues may also be triggered programmatically in situations where behavior cannot be defined graphically using Interface Builder.</p>
<p>The finished design is saved by Xcode to a storyboard file. Typically, an application will have a single storyboard file, though there is no restriction preventing the use of multiple storyboard files within a single application.</p>
<p>The remainder of this chapter will work through the creation of a simple application using storyboarding to implement multiple scenes with segues defined to allow user navigation.</p>
<h2><span style="color: #008080;">Adding Scenes</span></h2>
<p>To add a second scene to the storyboard, simply drag a view controller object from the Object library panel onto the canvas.</p>
<p>Drag and drop a label and a button into the second scene and configure the objects so that the view appears as follows:</p>
<p>As many scenes as necessary may be added to the storyboard, but for the purposes of this exercise will we use just two scenes. Having implemented the scenes the next step is to configure segues between the scenes.</p>
<h2><span style="color: #008080;">Configuring Storyboard Segues</span></h2>
<p><span style="color: #008080;">As previously discussed, a segue is the transition from one scene to another within a storyboard. Within the example application, touching the Go to Scene 2 button will segue to scene 2. Conversely, the button on scene 2 is intended to return the user to scene 1. To establish a segue, hold down the Ctrl key on the keyboard, click over a control (in this case the button on scene 1) and drag the resulting line to the scene 2 view. Upon releasing the mouse button a menu will appear. Select the modal menu option to establish the segue.</span></p>
<p><span style="color: #008080;">As more scenes are added to a storyboard, it becomes increasingly difficult to see more than a few scenes at one time on the canvas. To zoom out simply double click on the canvas. To zoom back in again simply double click once again on the canvas. Zoom buttons are also provided in the bottom right hand corner of the design canvas. Note that when zoomed out, it will not be possible to drag and drop items from the Object Library onto the scenes.</span></p>
<h2><span style="color: #008080;">Configuring Storyboard Transitions</span></h2>
<p><span style="color: #008080;">Xcode provides the option to change the visual appearance of the transition that takes place during a segue. By default a Cover Vertical transition is performed whereby the new scene slides vertically upwards from the bottom of the view to cover the currently displayed scene. To change the transition, select the corresponding segue line, display the attributes inspector (<em>View -&gt; Utilities -&gt; Show Attributes Inspector</em>) and modify the Transition setting. In Figure 21-6 the transition has been changed to Cross Dissolve.</span></p>
<p>If animation is not required during the transition, turn off the Animates option. To delete a segue from a storyboard simply select the arrow and press the keyboard delete key.</p>
<p>Compile and run the application. Note that touching the &ldquo;Go to Scene 1&rdquo; button causes Scene 2 to appear.</p>
<h2><span style="color: #008080;">Associating a View Controller with a Scene</span></h2>
<p>At this point in the example we have two scenes but only one view controller (the one created by Xcode when we selected Single View Application). Clearly in order to be able to add any functionality behind scene 2 it too will need a view controller. The first step, therefore, is to add the files for a view controller to the project. Ctrl-click on the Storyboard target at the top of the project navigator panel and select New File&hellip; from the resulting menu. In the new file panel select Objective-C class and click Next to proceed. On the options screen verify that the Subclass of menu is set to UIViewController and that the Targeted for iPad and With XIB for user interface options are both deselected (since the view already exists in the storyboard there is no need for an NIB user interface file) and name the class Scene2ViewContoller.</p>
<p>Select the MainStoryboard.storyboard file in the project navigator panel and select the View Controller button located in the panel beneath the Scene 2 view as shown in Figure 21-7:</p>
<p>With the view controller for scene 2 selected within the storyboard canvas, display the Identity Inspector (<em>View -&gt; Utilities -&gt; Identity Inspector</em>) and change the Class from UIViewController to Scene2ViewController:</p>
<p>Scene 2 now has a view controller and corresponding source files where code may be written to implement any required functionality.</p>
<p>Select the label object in scene 2 and display the Assistant Editor. Make sure that the Scene2ViewController.h file is displayed in the editor and then establish an outlet for the label named scene2Label.</p>
<h2><span style="color: #008080;">Passing Data Between Scenes</span></h2>
<p>One of the most common requirements when working with storyboards involves the transfer of data from one scene to another during a segue transition. This is achieved using the prepareForSegue: method.</p>
<p>Before a segue is performed by the storyboard runtime environment, a call is made to the prepareForSegue: method of the current view controller. If any tasks need to be performed prior to the segue taking place simply implement this method in the current view controller and add code to perform any necessary tasks. Passed as an argument to this method is a segue object from which a reference to the destination view controller may be obtained and subsequently used to transfer data.</p>
<p>To see this in action, begin by selecting Scene2ViewController.h and adding a new data property:</p>
<pre>#import &lt;UIKit/UIKit.h&gt;

@interface Scene2ViewController&nbsp;: UIViewController
@property (strong, nonatomic) IBOutlet UILabel *scene2Label;
@property (strong, nonatomic) NSString *labelText;
@end
</pre>
<p>This property will hold the text to be displayed on the label when the storyboard transitions to this scene. As such, some code needs to be added to the viewDidLoad: method located in the Scene2ViewController.m file:</p>
<pre>- (void)viewDidLoad
{
    [super viewDidLoad];
    _scene2Label.text = _labelText;
}
</pre>
<p>Next, select the StoryboardViewController.h file and import the header file for the Scene2ViewController class:</p>
<pre>#import &lt;UIKit/UIKit.h&gt;
#import "Scene2ViewController.h"

@interface StoryboardViewController&nbsp;: UIViewController
@property (strong, nonatomic) IBOutlet UIButton *scene1Label;

@end
</pre>
<p>Finally, select the StoryboardViewController.m file and implement the segue methods as follows:</p>
<pre>-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    Scene2ViewController *destination = 
                  [segue destinationViewController];

    destination.labelText = @"Arrived from Scene 1";
}
</pre>
<p>All this method does is obtain a reference to the destination view controller and then assigns a string to the labelText property of the object so that it appears on the label.</p>
<p>Compile and run the application once again and note that when scene 2 is displayed the new label text appears. We have, albeit using a very simple example, transferred data from one scene to the next.</p>
<h2><span style="color: #008080;">Unwinding Storyboard Segues</span></h2>
<p>ntroduced in 2012 with iOS 6, Unwind Segues give you a way to &ldquo;unwind&rdquo; the navigation stack and specify a destination to go back to. The first time you use them, they can be confusing. In fact&nbsp;there&rsquo;s no other UI feature of iOS development that has caused more discussion in our office than Unwind Segues. In this post, I&rsquo;ll help you understand the fundamentals.</p>
<p>&nbsp;</p>
<p>Ever since I noticed the strange &ldquo;Exit&rdquo; outlet show up in Xcode, I&rsquo;ve wondered what it did. Most people probably did what I did the first time they saw it. I Ctrl-draged from a button to the &ldquo;Exit&rdquo; outlet and nothing happened. This is the beginning of the confusion for new developers. How do I get the &ldquo;Exit&rdquo; outlet to do anything?</p>
<p>I&rsquo;m going to setup a simple example of three UIViewControllers in a UINavigationController.</p>
<p>&nbsp;</p>
<p>I put a &ldquo;Next&rdquo; button on each view controller that goes from 1 to 3 in order. To do this I used &ldquo;Show&rdquo; or &ldquo;Push&rdquo; Segues as you normally would to go from one view controller to the next. I want to create an Unwind Segue that goes from 3 all the way to 1.&nbsp;I placed a &ldquo;Home&rdquo; button on UIViewController #3. When pressed, I want it to unwind all the way to #1.</p>
<p><strong>Enable Unwind Segues&nbsp;</strong></p>
<p>To enable the Unwind Segue you need to add some code first. There are two things confusing about this:</p>
<ul>
<li>It&rsquo;s backwards. Normally when you create an action outlet for a button, Interface Builder will create the code for you. &nbsp;In this case Ctrl-dragging from the &ldquo;Home&rdquo; button to the &ldquo;Exit&rdquo; outlet is expecting code to already be in your project.</li>
<li>Normally when you create an action outlet for a button, it puts the code in the same class that owns the button. For Unwind Segues, you need to put the code in the destination view controller. In my simple example the code needs to go in view controller #1 even though the button that is executing the segue is in #3.</li>
</ul>
<p>So let&rsquo;s look at what code we need to write for our Unwind Segue. I assigned a custom class for UIViewController #1 and added the following function:</p>
<p><strong>OBJECTIVE-C</strong></p>
<p>-(IBAction)prepareForUnwind:(UIStoryboardSegue *)segue {</p>
<p>}</p>
<p>The signature of this function is key to Interface Builder recognizing it. It must have a return value of IBAction and take one parameter of UIStoryboardSegue*. The name of the function does not matter. In fact, the function does not even have to do anything. It&rsquo;s just there as a marker of which UIViewController is the destination of the Unwind Segue. Of course, you could put code in this function if you wanted to. You can inspect the identifier of the segue or access the source view controller and get at any properties you may need. The source view controller in my case would be #3.</p>
<p>Now that I have the code in place, I can return to Interface Builder and add our Unwind Segue.</p>
<p><strong>Adding the Unwind Segue in Interface Builder</strong></p>
<p>&nbsp;</p>
<p>This time when you Ctrl-drag from the &ldquo;Home&rdquo; button to the &ldquo;Exit&rdquo; outlet, you will see a modal popup.</p>
<p>Interface Builder will search every UIViewController class in your project for functions with that particular signature and list them all in the pop-up. Select the function that we just added in the previous step.&nbsp;</p>
<p>Now you will see the unwind segue added to the #3 view controller:</p>
<p>&nbsp;</p>
<p>And that&rsquo;s all that it takes to get a simple Unwind Segue working. When you press the &ldquo;Home&rdquo; button, the runtime will walk the navigation stack backwards until it finds a UIViewController that implements the function I selected as my Unwind Segue Action (prepareForUnwind) and it will transition to that view controller.</p>
<p>&nbsp;</p>
<h2><span id="Triggering_a_Storyboard_Segue_Programmatically" class="mw-headline">Triggering a Storyboard Segue Programmatically</span></h2>
<p>In addition to wiring up controls in scenes to trigger a segue, it is also possible to initiate a preconfigured segue from within the application code. This can be achieved by assigning an identifier to the segue and then making a call to the performSegueWithIdentifier: method of the view controller from which the segue is to be triggered.</p>
<p>To set the identifier of a segue, select it in the storyboard canvas, display the Attribute Inspector (<em>View -&gt; Utilities -&gt; Show Attribute Inspector</em>) and set the value in the Identifier field.</p>
<p>Assuming a segue with the identifier of SegueToScene1, this could be triggered from within code as follows:</p>
<pre>[self performSegueWithIdentifier: @"SegueToScene1" 
             sender: self];</pre>
</div>
</div>
</div>
</div>
</div>
</div>