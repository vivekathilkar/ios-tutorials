<div class="page" title="Page 3">
<div class="layoutArea">
<div class="column">
<h2><span style="color: #008080;">Application States</span></h2>
<p><span style="color: #000000;">All&nbsp;<a style="color: #000000;" href="https://itunes.apple.com/us/genre/ios/id36?mt=8">iOS apps</a>&nbsp;developed to run on version 4.0 or newer has the ability to process in the background. This means, of course, that a developer has many more options for managing memory and streamlining their iOS apps. The operating system keeps constant communication with each iOS app, and passes information to allow the app an opportunity to make the necessary adjustments. It is important for every developer to understand the concept of application state, and the five most important application methods to implement for handling transitions.</span></p>
<h2>States</h2>
<p>Apps developed for early iOS versions (before iOS 4.0) supported three states:&nbsp;<em>non-running</em>,&nbsp;<em>inactive</em>, and&nbsp;<em>active</em>. An application delegate for pre-iOS 4.0 apps received two important method calls: applicationDidFinishLaunching and applicationWillTerminate. When an app received an applicationDidFinishLaunching message, it was an opportunity for information to be retrieved from the previous launch to restore the app to its last used state. The status, applicationWillTerminate, was used to notify the app when the app was preparing to shut down. This gave the developer an opportunity to save any unsaved data or specific state information.</p>
<p>Currently, there are five possible application states that would be cause for the app to prepare for a transition - such as a shutdown or moving to the background. In certain cases, an app might need to continue processing in the background. However, there is certainly no reason for the app to process any graphics, animations, or display-specific routines. The five states of an iOS app - as listed in the&nbsp;<a href="http://developer.apple.com/library/ios/#DOCUMENTATION/iPhone/Conceptual/iPhoneOSProgrammingGuide/ManagingYourApplicationsFlow/ManagingYourApplicationsFlow.html">iOS App Programming Guide</a>&nbsp;- include the following:</p>
<ol type="1">
<ol type="1">
<li><strong><em>Non-running</em></strong>&nbsp;- The app is not running.</li>
<li><strong><em>Inactive</em></strong>&nbsp;- The app is running in the foreground, but not receiving events. An iOS app can be placed into an inactive state, for example, when a call or SMS message is received.</li>
<li><strong><em>Active</em></strong>&nbsp;- The app is running in the foreground, and receiving events.</li>
<li><strong><em>Background</em></strong>&nbsp;- The app is running in the background, and executing code.</li>
<li><strong><em>Suspended</em></strong>&nbsp;- The app is in the background, but no code is being executed.</li>
</ol>
</ol>
<p>&nbsp;</p>
<h2>The seven most important application delegate methods</h2>
<p>The operating system calls specific methods within the application delegate to facilitate transitioning to and from various states. The seven most important application delegate methods a developer should handle are:</p>
<pre>application:willFinishLaunchingWithOptions</pre>
<div class="teads-inread">&nbsp;</div>
<p>Method called when the launch process is initiated. This is the first opportunity to execute any code within the app.</p>
<pre>application:didFinishLaunchingWithOptions</pre>
<p>Method called when the launch process is nearly complete. Since this method is called is before any of the app's windows are displayed, it is the last opportunity to prepare the interface and make any final adjustments.</p>
<pre>applicationDidBecomeActive</pre>
<p>Once the application has become active, the application delegate will receive a callback notification message via the method applicationDidBecomeActive.</p>
<div class="sharethrough-article" data-component="medusaContentRecommendation" data-medusa-content-recommendation-options="{&quot;promo&quot;:&quot;promo_TR_recommendation_sharethrough_blog_page_desktop&quot;,&quot;spot&quot;:&quot;dfp-blog&quot;}">&nbsp;</div>
<p>This method is also called each time the app returns to an active state from a previous switch to inactive from a resulting phone call or SMS.</p>
<pre>applicationWillResignActive</pre>
<p>There are several conditions that will spawn the applicationWillResignActive method. Each time a temporary event, such as a phone call, happens this method gets called. It is also important to note that "quitting" an iOS app does not terminate the processes, but rather moves the app to the background.</p>
<pre>applicationDidEnterBackground</pre>
<p>This method is called when an iOS app is running, but no longer in the foreground. In other words, the user interface is not currently being displayed. According to Apple's&nbsp;<a href="http://developer.apple.com/library/ios/#documentation/uikit/reference/UIApplicationDelegate_Protocol/Reference/Reference.html">UIApplicationDelegate Protocol Reference</a>, the app has approximately five seconds to perform tasks and return. If the method does not return within five seconds, the application is terminated.</p>
<pre>applicationWillEnterForeground</pre>
<p>This method is called as an app is preparing to move from the background to the foreground. The app, however, is not moved into an active state without the applicationDidBecomeActive method being called. This method gives a developer the opportunity to re-establish the settings of the previous running state before the app becomes active.</p>
<pre>applicationWillTerminate</pre>
<p>This method notifies your application delegate when a termination event has been triggered. Hitting the home button no longer quits the application. Force quitting the iOS app, or shutting down the device triggers the applicationWillTerminate method. This is the opportunity to save the application configuration, settings, and user preferences.</p>
<h2>Application state changes</h2>
<p>Every iOS app is always in one of the five app states. The operating system manages the app state, but the app itself is responsible for managing important tasks to ensure smooth transitions between the states. Developers are required to respond appropriately to app state transitions.</p>
<p>With multitasking capability, the latest iOS version manages the resources available to every app. It is important to note, however, that the operating system limits what an app can do in the background. If an app needs to continue running in the background (with limited functionality), you must request permission.</p>
<h2>Launching the app</h2>
<p>The moment a user taps the app icon, the app begins to change state. The app delegate receives an application:willFinishLaunchingWithOptions method call, and the app state changes from&nbsp;<em>non-running</em>&nbsp;to&nbsp;<em>inactive</em>. Once in the&nbsp;<em>inactive</em>&nbsp;state, the app delegate will receive an application:didFinishLaunchingWithOptions method call, giving the app an opportunity to make final adjustments before the interface is displayed. If the app has not been designed to launch in the background, the operating system will activate the app, set the app state to&nbsp;<em>active</em>, and send the applicationDidBecomeActive method call to the app delegate.</p>
<h2>Interruptions</h2>
<p>On occasion, the iOS app will need to respond to interruptions. An alert-based interruption - such as a phone call - causes the app to move into an&nbsp;<em>inactive</em>&nbsp;state. The app delegate will receive an applicationWillResignActive method call, allowing the app an opportunity to prepare for a temporary&nbsp;<em>inactive</em>&nbsp;state. If the user chooses to ignore the interruption, or the interrupting process has terminated, the app will move back into the&nbsp;<em>active</em>&nbsp;state. While making the transition from&nbsp;<em>inactive</em>to&nbsp;<em>active</em>, the app delegate will receive an applicationDidBecomeActive method call.</p>
<h2>Switching to the background</h2>
<p>The iOS devices make it simple to quickly switch from app to app; when a user switches to a different app, the current app moves to the background. The app can be in one of two states:&nbsp;<em>background</em>&nbsp;or&nbsp;<em>suspended</em>. In either case, and before switching to the background, the app delegate receives an applicationWillResignActive method call, followed by an applicationDidEnterBackground message. If in a&nbsp;<em>suspended</em>&nbsp;state, the app sleeps. A background state - meaning that the app is allowed to continue executing code - requires the app to monitor and handle events. Developers need to be aware that the operating system may terminate the app at any time.</p>
<h2>One final thought</h2>
<p>Apple's iOS is among the most intuitive and user-friendly operating systems available. As long as developers follow best practices, the user experience remains consistent and trouble free. Highly evolved apps take full advantage of the constant communication between the operating system and the iOS app. It takes very little effort to monitor the transition messages and handle the various app states. A little due diligence goes a long way.</p>