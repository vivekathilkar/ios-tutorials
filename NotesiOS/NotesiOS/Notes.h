//
//  Notes.h
//  NotesiOS
//
//  Created by vivek athilkar on 19/12/16.
//  Copyright © 2016 vivek athilkar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FZAccordionTableView.h"
#import "AccordionHeaderView.h"

@interface Notes : UIViewController <UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,UIScrollViewDelegate>

{
    int selectedIndex;
    NSMutableArray *titleArray;
    NSArray *subtitleArray;
    NSArray *textArray;
    UIButton *buttoncell;
    NSString *selectedRow;
    

}

@property (strong,readonly,nonatomic) IBOutlet FZAccordionTableView *tableView;

@property (nonatomic,retain) NSMutableArray *mutableArray;
@property (nonatomic,retain) NSMutableArray *tableDataArray;
//SUBTITLE ARRAY
@property (nonatomic,retain) NSMutableArray *introduction;
@property (nonatomic,retain) NSMutableArray *xcode;
@property (nonatomic,retain) NSMutableArray *objectiveC;
@property (nonatomic,retain) NSMutableArray *appPattern;
@property (nonatomic,retain) NSMutableArray *viewWindows;
@property (nonatomic,retain) NSMutableArray *storyBoardss;
@property (nonatomic,retain) NSMutableArray *tableViewss;
@property (nonatomic,retain) NSMutableArray *navigationBased;
@property (nonatomic,retain) NSMutableArray *Uipicker;
@property (nonatomic,retain) NSMutableArray *directoriesFile;
@property (nonatomic,retain) NSMutableArray *workingWithdata;
@property (nonatomic,retain) NSMutableArray *multiTouchTap;
@property (nonatomic,retain) NSMutableArray *drawing;
@property (nonatomic,retain) NSMutableArray *animation;
@property (nonatomic,retain) NSMutableArray *multasking;
@property (nonatomic,retain) NSMutableArray *notifications;
@property (nonatomic,retain) NSMutableArray *corelocations;
@property (nonatomic,retain) NSMutableArray *consurrency;
@property (nonatomic,retain) NSMutableArray *networking;
@property (nonatomic,retain) NSMutableArray *targetingDevices;
@property (nonatomic,retain) NSMutableArray *liclization;
@property (nonatomic,retain) NSMutableArray *PerfPowerOpt;

//FKEY ARRAY
@property (nonatomic,retain) NSMutableArray *stpkfintroduction;
@property (nonatomic,retain) NSMutableArray *stpkxcode;
//@property (nonatomic,retain) NSMutableArray *stpkobjectiveC;
//@property (nonatomic,retain) NSMutableArray *stpkappPattern;
//@property (nonatomic,retain) NSMutableArray *stpkviewWindows;
//@property (nonatomic,retain) NSMutableArray *stpkstoryBoards;
//@property (nonatomic,retain) NSMutableArray *stpktableView;
//@property (nonatomic,retain) NSMutableArray *stpknavigationBased;
//@property (nonatomic,retain) NSMutableArray *stpkUipicker;
//@property (nonatomic,retain) NSMutableArray *stpkdirectoriesFile;
//@property (nonatomic,retain) NSMutableArray *stpkworkingWithdata;
//@property (nonatomic,retain) NSMutableArray *stpkmultiTouchTap;
//@property (nonatomic,retain) NSMutableArray *stpkdrawing;
//@property (nonatomic,retain) NSMutableArray *stpkanimation;
//@property (nonatomic,retain) NSMutableArray *stpkmultasking;
//@property (nonatomic,retain) NSMutableArray *stpknotifications;
//@property (nonatomic,retain) NSMutableArray *stpkcorelocations;
//@property (nonatomic,retain) NSMutableArray *stpkconsurrency;
//@property (nonatomic,retain) NSMutableArray *stpknetworking;
//@property (nonatomic,retain) NSMutableArray *stpktargetingDevices;
//@property (nonatomic,retain) NSMutableArray *stpkliclization;



@end
