//
//  Question.h
//  NotesiOS
//
//  Created by vivek athilkar on 16/12/16.
//  Copyright © 2016 vivek athilkar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <DLRadioButton.h>
#import "CustomIOSAlertView.h"

@interface Question : UIViewController
{
    NSInteger a;
    BOOL attempted;
}

@property (nonatomic,assign) NSInteger score;
@property (nonatomic,assign) NSInteger scorecopy;
@property (nonatomic,assign) NSInteger levelButtonTag;
@property (nonatomic,strong) NSMutableDictionary *questionDetail;
@property (nonatomic,strong) NSMutableDictionary *questionAttempted;
//@property (nonatomic,strong) NSMutableArray *Keys;


+(Question *) thePlayer;
+(void)resetSharedInstance;
-(void)buttonTag:(UIButton *)button;

-(void)showNextQuestion :(NSInteger *)maxAllowedQuestions questionArray:(NSArray *)questionArray;
@property (nonatomic,strong) NSString *str;

@property (weak, nonatomic) IBOutlet UIButton *nextButton;




@end
